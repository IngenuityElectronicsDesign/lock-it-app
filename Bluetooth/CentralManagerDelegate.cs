﻿using System;
using CoreBluetooth;
using Foundation;

namespace LockIt 
{
	public class CentralManagerDelegate : CBCentralManagerDelegate
	{
		public event Action<bool> DidChangeToPowerOnEvent;
		public event Action<bool> DidChangeToConnectedEvent;
		public event Action<CBPeripheral> DiscoveredPeripheralEvent;
		public event Action <CBPeripheral> ConnectedPeripheralEvent;

		public override void UpdatedState(CBCentralManager central)
		{
			if (central.State == CBCentralManagerState.PoweredOn) {
				if (DidChangeToPowerOnEvent != null) {
					DidChangeToPowerOnEvent (true);
				}
			} else if (central.State == CBCentralManagerState.PoweredOff) {
				if (DidChangeToConnectedEvent != null) {
					DidChangeToConnectedEvent (false);
				}
				if (DidChangeToPowerOnEvent != null) {
					DidChangeToPowerOnEvent (false);
				}
			}
		}

		public override void WillRestoreState(CBCentralManager central, NSDictionary dict)
		{
			Console.WriteLine(">> Restore State");

			//			NSDictionary dicti = (NSDictionary)dict[CBCentralManager.RestoredStatePeripheralsKey];
			//
			//			for( int i = 0; i < dict.Count; i++ )// CBPeripheral peripheral in e.Dict[CBCentralManager.RestoredStatePeripheralsKey] )
			//			{
			//				if( peripheral.State == CBPeripheralState.Connected )
			//				{
			//					ConnectedPeripheral = peripheral;
			//					// Delegate?
			//				}
			//			}
		}

		public override void DiscoveredPeripheral(CBCentralManager central, CBPeripheral peripheral, NSDictionary advertisementData, NSNumber RSSI)
		{
			Console.WriteLine(">> Discovered Peripheral");

			if( peripheral != null ) // TODO: Filter by device UUID
			{
				Console.WriteLine (peripheral.Description + " : " + peripheral.Identifier.ToString ());
				// Ensure we haven't already connected to the device
				if( peripheral.State != CBPeripheralState.Connected )
				{
					if (DiscoveredPeripheralEvent != null) {
						DiscoveredPeripheralEvent (peripheral);
					}
//					NSDictionary options = NSDictionary.FromObjectAndKey( NSNumber.FromBoolean(true), CBCentralManager.OptionNotifyOnDisconnectionKey );
//					BLEInterface.Instance.ConnectedPeripheral = peripheral;
//					central.ConnectPeripheral(peripheral, options);
				}
				else
				{
					Console.WriteLine("Peripheral: {0}  Connection State: {1}", peripheral, peripheral.State);
				}
			}
		}

		public override void ConnectedPeripheral(CBCentralManager central, CBPeripheral peripheral)
		{
			Console.WriteLine(">> Connected Peripheral");

//			peripheral.DiscoveredService += (sender, e) => // Discovered Services
//			{
//				if( e.Error == null )
//				{
//					Console.WriteLine("Discovering services for Peripheral \"{0}\" (UUID: {1})", peripheral.Name, peripheral.Identifier);
//
//					BLEInterface.Instance.CharacteristicsIter = 0;
//
//					foreach( CBService service in peripheral.Services )
//					{
//						// TODO: Possibly filter this
//						peripheral.DiscoverCharacteristics((CBUUID[])null, service);
//					}
//				}
//				else
//				{
//					Console.WriteLine("Services discovery failed with error: {0}", e.Error);
//				}
//			};

//			peripheral.DiscoveredCharacteristic += (sender, e) => // Discovered Characteristic
//			{
//				Console.WriteLine(">> Discovered Characteristics");
//
//				if( e.Error == null )
//				{
//					BLEInterface.Instance.PeripheralCharacteristicDict.Clear();
//
//					foreach( CBCharacteristic characteristic in e.Service.Characteristics )
//					{
//						int detectedUUID = -1;
//
//						if( characteristic.UUID == BLEInterface.Instance.IntToCBUUID(BLEInterface.SendMsgUUID) )
//						{
//							detectedUUID = BLEInterface.SendMsgUUID;
//						}
//						else if( characteristic.UUID == BLEInterface.Instance.IntToCBUUID(BLEInterface.RecvMsgUUID) )
//						{
//							detectedUUID = BLEInterface.RecvMsgUUID;
//						}
//						else if( characteristic.UUID == BLEInterface.Instance.IntToCBUUID(BLEInterface.DebugWriteUUID) )
//						{
//							detectedUUID = BLEInterface.DebugWriteUUID;
//						}
//						else if( characteristic.UUID == BLEInterface.Instance.IntToCBUUID(BLEInterface.DebugReadUUID) )
//						{
//							detectedUUID = BLEInterface.DebugReadUUID;
//						}
//
//						if( detectedUUID != -1 )
//						{
//							peripheral.SetNotifyValue(true, characteristic);
//
//							BLEInterface.Instance.PeripheralCharacteristicDict.Add(detectedUUID, characteristic);
//
//							BLEInterface.Instance.CharacteristicsIter++;
//
//							if( BLEInterface.Instance.CharacteristicsIter == BLEInterface.TotalNumCharacteristics )
//							{
//								BLEInterface.Instance.IsConnected = true;
//								if(DidChangeToConnectedEvent != null) {
//									DidChangeToConnectedEvent(BLEInterface.Instance.IsConnected);
//								}
//							}
//						}
//					}
//				}
//				else
//				{
//					Console.WriteLine("Characteristics discovery failed with error: {0}", e.Error);
//				}
//			};
//
//			peripheral.UpdatedCharacterteristicValue += (sender, e) => // Updated Characteristic Value
//			{
//				Console.WriteLine(">> Update Characteristics\n");
//
//				if( e.Error == null )
//				{
//					if( e.Characteristic.UUID == BLEInterface.Instance.IntToCBUUID(BLEInterface.RecvMsgUUID) )
//					{
//						byte[] bytes = new byte[e.Characteristic.Value.Length];
//						System.Runtime.InteropServices.Marshal.Copy(e.Characteristic.Value.Bytes, bytes, 0, Convert.ToInt16(e.Characteristic.Value.Length));
//						object header = BLEInterface.Instance.DeserializeStruct<BLEInterface.TBLEProtocolHeader>(bytes);
//
//						switch( ((BLEInterface.TBLEProtocolHeader)header).Cmd )
//						{
//						//						case (byte)BLEInterface.BLEReply.StatusRequest:
//						//							{
//						//								object d = BLEInterface.Instance.DeserializeStruct<BLEInterface.TBLERPLStatusRequest>(bytes);
//						//								BLEInterface.TBLERPLStatusRequest status = (BLEInterface.TBLERPLStatusRequest)d;
//						//
//						//								BLEInterface.Instance.Delegate.DidGetStatus(status.BatteryLevel, status.StoredEventsLogNum, status.VerMajor, status.VerMinor);
//						//
//						//								break;
//						//							}
//						//
//						//						case (byte)BLEInterface.BLEReply.ParamValue:
//						//							{
//						//								object d = BLEInterface.Instance.DeserializeStruct<BLEInterface.TBLERPLParamValue>(bytes);
//						//								BLEInterface.TBLERPLParamValue reply = (BLEInterface.TBLERPLParamValue)d;
//						//
//						//								BLEInterface.Instance.Delegate.DidGetParamValue(System.Text.Encoding.UTF8.GetString(reply.Name).Trim(new char[]{'\0'}), reply.Value);
//						//
//						//								Console.WriteLine("Param Value Reply: {0} {1}", System.Text.Encoding.UTF8.GetString(reply.Name), reply.Value.ToString());
//						//
//						//								break;
//						//							}
//						//
//						//						case (byte)BLEInterface.BLEReply.ParamSetAck:
//						//							{
//						//								BLEInterface.Instance.Delegate.DidGetSetParamAck();
//						//
//						//								Console.WriteLine("Param Set Value Ack");
//						//
//						//								break;
//						//							}
//						//
//						//						case (byte)BLEInterface.BLEReply.EventsLogEntry:
//						//							{
//						//								object d = BLEInterface.Instance.DeserializeStruct<BLEInterface.TBLERPLEventsLogEntry>(bytes);
//						//								BLEInterface.TBLERPLEventsLogEntry entry = (BLEInterface.TBLERPLEventsLogEntry)d;
//						//
//						//								BLEInterface.Instance.Delegate.DidGetEventsLogEntry(entry.TimeStamp, entry.TimeInZone, entry.TagID, entry.EventType);
//						//
//						//								break;
//						//							}
//						}
//					}
//					else if( e.Characteristic.UUID == BLEInterface.Instance.IntToCBUUID(BLEInterface.DebugReadUUID) )
//					{
//						Console.WriteLine("Debug message received: {0}", NSString.FromData(e.Characteristic.Value, NSStringEncoding.UTF8));
//					}
//				}
//				else
//				{
//					Console.WriteLine("Characteristics value update failed with error: {0}", e.Error);
//				}
//			};

//			peripheral.WroteCharacteristicValue += (sender, e) => // Wrote Characteristic Value
//			{
//				if( e.Error == null )
//				{
//					Console.WriteLine("Characteristics value written");
//				}
//				else
//				{
//					Console.WriteLine("Characteristics value write failed with error: {0}", e.Error);
//				}
//			};
//
//			peripheral.RssiUpdated += (sender, e) => // RSSI Updated
//			{
//				//BLEMasterInterface.Instance.Delegate.DeviceRSSIChanged(peripheral.RSSI.FloatValue);
//				Console.WriteLine("Peripheral RSSI Changed: {0}", peripheral.RSSI.FloatValue);
//			};

			if( peripheral.Identifier != null )
			{
				Console.WriteLine("Connected to Peripheral with UUID: {0}", peripheral.Identifier);
			}
			else
			{
				Console.WriteLine("Connected to Peripheral with unknown UUID");
			}

//			CBUUID[] services = { BLEInterface.Instance.IntToCBUUID(BLEInterface.ServiceUUID) };
//			peripheral.DiscoverServices(services);

			BLEInterface.Instance.StopScanForPeripherals();

			if (ConnectedPeripheralEvent != null) {
				ConnectedPeripheralEvent (peripheral);
			}
		}

		public override void DisconnectedPeripheral(CBCentralManager central, CBPeripheral peripheral, NSError error)
		{
			Console.WriteLine(">> Disconnected Peripheral");
			if (DidChangeToConnectedEvent != null) {
				DidChangeToConnectedEvent (false);
			}
		}
	}
}