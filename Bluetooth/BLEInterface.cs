﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Foundation;
using CoreBluetooth;
using System.Timers;
using System.Text;

namespace LockIt
{
	public class BLEInterface
	{
		#region Interface Defs

		// Define Peripherals
		public const int PeripheralUUID = 0x1111;

		// Define Services
		public const int ServiceUUID = 0xAAA0;

//		// Define Characteristics for BLE Service
//		public const int SendMsgUUID = 0x2A11;
//		public const int RecvMsgUUID = 0x2A13;

		public const string SendMsgUUID = "68e53b11-0000-11e5-885d-feff819cdc9f";
		public const string RecvMsgUUID = "68e53b12-0000-11e5-885d-feff819cdc9f";

		// Define Debug Characteristics for BLE Service
		public const int DebugWriteUUID = 0x2A10;
		public const int DebugReadUUID = 0x2A12;

		public const int TotalNumCharacteristics = 4;



		#endregion Interface Defs

		#region Protocol Defs

		// Protocol Consts
		public const int MaxParamNameLength = 14;
		public const int MaxParamValueLength = 4;
		public const int MaxDeviceNameLength = 18;
		public const int MaxGenericErrStr = 18;

		public const int MaxBLEPacketLength = 20;

		public const int PayloadInStartMsgLen = MaxBLEPacketLength - 2 - 1 - 1;
		public const int PayloadInDataMsgLen = MaxBLEPacketLength - 1 - 1;

		// Generic Protocol Header
		public struct TBLEProtocolHeader
		{
			public byte Cmd;
		}

		//
		// Host to Device Messages
		//

		public enum BLELockItCommand : byte
		{
			AdminCabinetUnlock = 1,
			CabinetIsDoorOpen = 2
		}

		public enum BLELockitCabinetCommand : byte
		{
			PacketStart = 1,
			PacketData = 2
		}

		[StructLayout (LayoutKind.Sequential, Pack = 1)]
		public struct TLogicalPacketStart
		{
			public TBLEProtocolHeader Header;
			public byte Filler;
			public UInt16 TotalDataLen;
			[MarshalAs (UnmanagedType.ByValArray, SizeConst = PayloadInStartMsgLen)]
			public byte[] Payload;
		}

		public struct TLogicalPacketData
		{
			public TBLEProtocolHeader Header;
			public byte SeqNum;
			[MarshalAs (UnmanagedType.ByValArray, SizeConst = PayloadInDataMsgLen)]
			byte[] Payload;
		}

		public enum BLELockItCabinetReply : byte
		{
			Status = 1
		}

		public enum BLELockItReply : byte
		{
			Ack = 1,
			Nack = 2,
			CabinetIsDoorOpen = 3
		}

		[StructLayout (LayoutKind.Sequential, Pack = 1)]
		public struct TReplyStatus
		{
			public TBLEProtocolHeader Header;
			public UInt16 VerMajor;
			public UInt16 VerMinor;
		}

		public enum LockitCabinetOpenStatus : byte {
			Closed = 0,
			Open = 1
		}

		[StructLayout (LayoutKind.Sequential, Pack = 1)]
		public struct TLockItReplyCabinetIsDoorOpen
		{
			public TBLEProtocolHeader Header;
			public byte OpenStatus;
		}

		#endregion Protocol Defs

		#region Singleton Instance

		private static BLEInterface m_Instance;

		public static BLEInterface Instance {
			get {
				if (m_Instance == null) {
					m_Instance = new BLEInterface ();
				}
				return m_Instance;
			}
		}

		#endregion Singeton Instance

		public bool IsConnected { get { return ConnectedPeripheral != null; } }
		public bool IsReadyToSend {
			get {
				return ConnectedPeripheral != null && PeripheralCharacteristicDict != null && PeripheralCharacteristicDict.ContainsKey(SendMsgUUID);
			}
		}

		public CBCentralManager CentralManager { get; private set; }

		public CBPeripheral ConnectedPeripheral { get; private set; }

		public Dictionary<string,CBCharacteristic> PeripheralCharacteristicDict { get; private set; }

		public Dictionary<string, CBPeripheral> DiscoveredPeripherals = new Dictionary<string, CBPeripheral> ();

		CentralManagerDelegate CentralManagerDelegate;

		public int CharacteristicsIter { get; set; }

		public event Action Connected;
		public event Action Disconnected;

		public event Action<string> PeripheralDiscovered;

		public event Action<float> ConnectedPeripheralRSSIUpdated;

		public event Action<TLockItReplyCabinetIsDoorOpen> CabinetDoorIsOpenResponseReceived;

		private BLEInterface ()
		{
			PeripheralCharacteristicDict = new Dictionary<string, CBCharacteristic> ();

			CentralManagerDelegate = new CentralManagerDelegate ();
			CentralManagerDelegate.DidChangeToConnectedEvent += HandleDidChangeToConnected;
			CentralManagerDelegate.DidChangeToPowerOnEvent += HandleDidChangeToPowerOn;
			CentralManagerDelegate.DiscoveredPeripheralEvent += CentralManagerDelegate_DiscoveredPeripheralEvent;
			CentralManagerDelegate.ConnectedPeripheralEvent += CentralManagerDelegate_ConnectedPeripheralEvent;

			CentralManager = new CBCentralManager (CentralManagerDelegate, CoreFoundation.DispatchQueue.DefaultGlobalQueue, NSDictionary.FromObjectsAndKeys (new NSObject[] {
				new NSString ("BetachekCentralManager"),
				NSNumber.FromBoolean (true)
			}, new NSObject[] {
				CBCentralManager.OptionRestoreIdentifierKey,
				CBCentralManager.OptionShowPowerAlertKey
			}));
		}

		void CentralManagerDelegate_ConnectedPeripheralEvent (CBPeripheral peripheral)
		{
			ConnectedPeripheral = peripheral;

			peripheral.DiscoveredService += (sender, e) => { // Discovered Services
				if (e.Error == null) {
					Console.WriteLine ("Discovering services for Peripheral \"{0}\" (UUID: {1}) Length : {2}", peripheral.Name, peripheral.Identifier, peripheral.Services.Length);
			
					BLEInterface.Instance.CharacteristicsIter = 0;
			
					foreach (CBService service in peripheral.Services) {
						// TODO: Possibly filter this
						Console.WriteLine("Service Found : " + service.Description + " : " + service.UUID.ToString());
						peripheral.DiscoverCharacteristics ((CBUUID[])null, service);
					}
				} else {
					Console.WriteLine ("Services discovery failed with error: {0}", e.Error);
				}
			};

			peripheral.DiscoveredCharacteristic += (sender, e) => {
				Console.WriteLine(">> Discovered Characteristics");

				if(e.Error == null) {
					PeripheralCharacteristicDict.Clear();

					foreach(var c in e.Service.Characteristics) 
					{
						// TODO : Match up characteristics with Protocol once defined.
						Console.WriteLine("Characteristic : " + c.Description + " : " + c.UUID.ToString());
						this.PeripheralCharacteristicDict.Add(c.UUID.ToString(), c);
						if(c.UUID.ToString() == RecvMsgUUID) {
							peripheral.SetNotifyValue(true, c);
						}
						// Quick hack for testing
//						if(c.UUID.ToString() == SendMsgUUID && !hasRequested) {
//							requestTimer = new Timer(5000);
//							requestTimer.Elapsed += delegate {
//								RequestCanibetUnlock();
//								requestTimer.Close();
//								requestTimer = null;
//							};
//							requestTimer.Start();
//							hasRequested = true;
//
//							pollTimer = new Timer(1000);
//							pollTimer.Elapsed += delegate {
//								RequestCabinetIsDoorOpen();
//							};
//							pollTimer.Start();
//						}
					}
				}
				else
				{
					Console.WriteLine("Characteristics discovery failed with error: {0}", e.Error);
				}
			};

			peripheral.UpdatedCharacterteristicValue += (sender, e) => {
				Console.WriteLine(">> Update Characteristics\n");

				if(e.Error == null) {
					// TODO : Check characteristic based on Protocol UUID once defined 

					if(e.Characteristic.UUID.ToString() == RecvMsgUUID) {
						byte[] bytes = new byte[e.Characteristic.Value.Length];
						System.Runtime.InteropServices.Marshal.Copy(e.Characteristic.Value.Bytes, bytes, 0, Convert.ToInt16(e.Characteristic.Value.Length));

						Console.WriteLine("Characteristic Updated : " + e.Characteristic.UUID + " : " + BitConverter.ToString(bytes));
						// Check first byte for start or data
						if(bytes[0] == (byte)BLELockitCabinetCommand.PacketStart) {
							var packet = DeserializeStruct<TLogicalPacketStart>(bytes);
							var innerPacket = DeserializeStruct<TLockItReplyCabinetIsDoorOpen>(packet.Payload);
							if(CabinetDoorIsOpenResponseReceived != null) {
								CabinetDoorIsOpenResponseReceived(innerPacket);
							}
							Console.WriteLine("Lock status : " + ((int)innerPacket.OpenStatus).ToString());
						} else if (bytes[0] == (byte)BLELockitCabinetCommand.PacketData) {
							// TODO : Multi packet data
						}
					}
				} else {
					Console.WriteLine("Characteristics value update failed with error: {0}", e.Error);
				}
			};

			peripheral.UpdatedValue += (sender, e) => {
				Console.WriteLine("Updated Value");
			};

			peripheral.WroteCharacteristicValue += (sender, e) => {
				if( e.Error == null )
				{
					Console.WriteLine("Characteristics value written : " + e.Characteristic.UUID);
				}
				else
				{
					Console.WriteLine("Characteristics value write failed with error: {0}", e.Error);
				}
			};

			peripheral.RssiRead += (sender, e) => {
				Console.WriteLine("Connected Peripheral RSSI Updated: {0}", e.Rssi);
				if(ConnectedPeripheralRSSIUpdated != null) {
					ConnectedPeripheralRSSIUpdated(e.Rssi.FloatValue);
				}
			};

			peripheral.DiscoverServices ();

			if (Connected != null) {
				Connected ();
			}
		}

		void CentralManagerDelegate_DiscoveredPeripheralEvent (CBPeripheral peripheral)
		{
			// TODO : Deal with same name but custom advertising data once implemented on devices.	
			if (peripheral.Name != null) {
				if (!DiscoveredPeripherals.ContainsKey (peripheral.Name)) {
					DiscoveredPeripherals.Add (peripheral.Name, peripheral);
				}
				if (PeripheralDiscovered != null) {
					PeripheralDiscovered (peripheral.Name);
				}
			}
		}

		void HandleDidChangeToPowerOn (bool turnedOn)
		{
			if (turnedOn) {
				
			} else {
				DiscoveredPeripherals.Clear ();
				ConnectedPeripheral = null;
				if (Disconnected != null) {
					Disconnected ();
				}
			}
		}

		void HandleDidChangeToConnected (bool connected)
		{
			if (connected && Connected != null) {
				Connected ();
			} else if (!connected && Disconnected != null) {
				ConnectedPeripheral = null;
				Disconnected ();
			}
		}

		// unsafe
		public bool ScanForPeripherals ()
		{
			if (CentralManager.State != CBCentralManagerState.PoweredOn) {
				return false;
			}

			Console.WriteLine (">> Scanning for Peripherals");

			// Clear existing discoveries
			DiscoveredPeripherals.Clear ();

			//NSArray services = NSArray.FromObject(IntTOCBUUID(SNServiceUUID));

//			CentralManager.ScanForPeripherals (IntToCBUUID (ServiceUUID));
			CentralManager.ScanForPeripherals(new CBUUID[]{});
			return true;
		}

		public void StopScanForPeripherals ()
		{
			if (CentralManager.State == CBCentralManagerState.PoweredOn) {
				CentralManager.StopScan ();
			}

			Console.WriteLine (">> Stopped scanning for Peripherals");
		}

		public bool ConnectToPeripheral (string uuid)
		{
			if (!DiscoveredPeripherals.ContainsKey (uuid)) {
				return false;
			}

			NSDictionary options = NSDictionary.FromObjectAndKey (NSNumber.FromBoolean (true), CBCentralManager.OptionNotifyOnDisconnectionKey);
			var p = DiscoveredPeripherals [uuid];
			CentralManager.ConnectPeripheral (p, options);
			return true;
		}

		void SendMessageToPeripheral (NSData data)
		{
			if (ConnectedPeripheral != null) {
//				if (data.Length > MaxBLEPacketLength) {
//					Console.WriteLine ("Error: Outgoing packet exceeds maximum size.");
//					return;
//				}
				if (!PeripheralCharacteristicDict.ContainsKey (SendMsgUUID)) {
					// Too early
					Console.WriteLine("Send Message Characteristic not discovered yet");
					return;
				}
				if (data.Length > MaxBLEPacketLength) {
					// Multiple Packets

				}
				TLogicalPacketStart start = new TLogicalPacketStart ();
				TBLEProtocolHeader h = new TBLEProtocolHeader ();
				h.Cmd = (byte)BLELockitCabinetCommand.PacketStart;
				start.Header = h;
				start.TotalDataLen = (ushort)data.Length;
				if (data.Length > PayloadInStartMsgLen) {
					// TODO : Split it up
				} else {
					start.Payload = data.ToArray ();
				}

				var wrappedBytes = SerializeStruct<TLogicalPacketStart> (start);

				CBCharacteristic characteristic = PeripheralCharacteristicDict [SendMsgUUID];

				if (ConnectedPeripheral.State == CBPeripheralState.Connected && characteristic != null) {
					ConnectedPeripheral.WriteValue (NSData.FromArray(wrappedBytes), characteristic, CBCharacteristicWriteType.WithResponse);
				}
			} else {
				Console.WriteLine ("Peripheral Not Connected");
			}
		}

//		public void RequestStatus ()
//		{
//			if (ConnectedPeripheral != null) {
//				TBLEProtocolHeader Packet = new TBLEProtocolHeader ();
//				Packet.Cmd = (byte)BLELockitCabinetCommand.StatusRequest;
//				byte[] bytes = SerializeStruct<TBLEProtocolHeader> (Packet);
//				SendMessageToPeripheral (NSData.FromArray (bytes));
//			} else {
//				Console.WriteLine ("RequestStatus : Not connected to a peripheral.");
//			}
//		}

		public void RequestRSSIUpdates() {
			if (ConnectedPeripheral != null) {
				ConnectedPeripheral.ReadRSSI ();
			} else {
				Console.WriteLine ("RequestRSSIUpdates : Not connected to a peripheral.");
			}
		}

		public void RequestCabinetIsDoorOpen() {
			if (ConnectedPeripheral != null) {
				Console.WriteLine ("Requesting Cabinet Is Door Open");
				SendBasicCommand ((byte)BLELockItCommand.CabinetIsDoorOpen);
			} else {
				Console.WriteLine ("RequestCabinetIsLocked : Not connected to a peripheral.");
			}
		}

		public void RequestCanibetUnlock() {
			if (ConnectedPeripheral != null) {
				Console.WriteLine ("Requesting Cabinet Unlock");
				SendBasicCommand ((byte)BLELockItCommand.AdminCabinetUnlock);
			} else {
				Console.WriteLine ("RequestCanibetUnlock : Not connected to a peripheral.");
			}
		}

		private void SendBasicCommand(byte cmd) {
			TBLEProtocolHeader packet = new TBLEProtocolHeader ();
			packet.Cmd = cmd;

			byte[] bytes = SerializeStruct<TBLEProtocolHeader> (packet);
			SendMessageToPeripheral (NSData.FromArray (bytes));
		}

		public void DisconnectFromCurrentPeripheral() {
			if (ConnectedPeripheral != null) {
				CentralManager.CancelPeripheralConnection (ConnectedPeripheral);
			}
		}




		#region Utility Funcs

		public CBUUID IntToCBUUID (UInt16 uuid)
		{
			UInt16 temp = (UInt16)(uuid << 8);
			temp |= (UInt16)(uuid >> 8);

			byte[] byteArray = BitConverter.GetBytes (temp);
			NSData data = NSData.FromStream (new System.IO.MemoryStream (byteArray));
			return CBUUID.FromData (data);
		}

		// Convert a Struct into a Byte stream
		public byte[] SerializeStruct<T> (T st) where T : struct
		{
			int objsize = Marshal.SizeOf (typeof(T));
			byte[] ret = new Byte[objsize];
			IntPtr buff = Marshal.AllocHGlobal (objsize);
			Marshal.StructureToPtr (st, buff, false);
			Marshal.Copy (buff, ret, 0, objsize);
			Marshal.FreeHGlobal (buff);
			return ret;
		}

		// Convert a Byte stream into a Struct
		public T DeserializeStruct<T> (byte[] data) where T : struct
		{
			int objsize = Marshal.SizeOf (typeof(T));
			IntPtr buff = Marshal.AllocHGlobal (objsize);
			Marshal.Copy (data, 0, buff, objsize);
			T ret = (T)Marshal.PtrToStructure (buff, typeof(T));
			Marshal.FreeHGlobal (buff);
			return ret;
		}

		#endregion Utility Funcs
	}
}
