// LockItCabinet project - BLE commands definitions
// By Ran Peleg, Ingenuity
// Date: April, 2015
// ----------------------------------------------------------------------------

#ifndef _LOCKITCABINET_BLE_PROTOCOL_DEFS_H_
#define _LOCKITCABINET_BLE_PROTOCOL_DEFS_H_

#include "Common/CommonTypes.h"


#undef PACKED_STRUCT

// Align all structures to byte boundry
#ifdef __GNUC__
// if GCC or LLVM compilers
#  define PACKED_STRUCT __attribute__ ((packed))
#else
// If microsoft or IAR compilers
#  pragma pack(push,1)
#  define PACKED_STRUCT
#  define ALIGN_POP_IS_REQUIRED
#endif

#define MAX_BLE_PACKET_LENGTH 20

// Generic protocol header
struct TLockItCabinetBleProtocolHeader {
  U8 Cmd;
};

// Host to device messages
// ------------------------------------------------------------------

// Status request commamd (no message)
#define LOCKITCABINET_BLE_CMD_STATUS_REQUEST 1

// Logical data packet start
#define LOCKITCABINET_BLE_CMD_PACKET_START 2

// Maximum payload size inside the start packet
#define PAYLOAD_IN_START_MSG_LEN (MAX_BLE_PACKET_LENGTH - 2 - 1 - 1)

struct TLogicalPacketStart {
  struct TLockItCabinetBleProtocolHeader Header;
  U8 Filler;
  U16 TotalDataLen;
  U8 Payload[PAYLOAD_IN_START_MSG_LEN];
} PACKED_STRUCT;

// Logical data payload
#define LOCKITCABINET_BLE_CMD_PACKET_DATA 3

#define PAYLOAD_IN_DATA_MSG_LEN (MAX_BLE_PACKET_LENGTH - 1 - 1)

struct TLogicalPacketData {
  struct TLockItCabinetBleProtocolHeader Header;
  U8 SeqNum;
  U8 Payload[PAYLOAD_IN_DATA_MSG_LEN];
} PACKED_STRUCT;

// Device to host messages
// ------------------------------------------------------------------

// Status reply
#define LOCKITCABINET_BLE_REPLY_STATUS 1

struct TReplyStatus {
  struct TLockItCabinetBleProtocolHeader Header;
  U16 VerMajor;
  U16 VerMinor;
} PACKED_STRUCT;

// Restore previous align settings
#ifdef ALIGN_POP_IS_REQUIRED
#  pragma pack(pop)
#endif

#endif
