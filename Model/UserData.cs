﻿using System;

namespace LockIt
{
	public class UserData
	{
		public enum AccessLevels
		{
			SystemManager,
			StockManager,
			Staff
		}

		public string Name { get; private set; }
		public string Pass { get; private set; }
		public string Pin { get; private set; }
		public AccessLevels AccessLevel { get; private set; }

		public UserData(string name, string pass, AccessLevels accessLevel)
		{
			Name = name;
			Pass = pass;
			AccessLevel = accessLevel;
		}
	}
}

