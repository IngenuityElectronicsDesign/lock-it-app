﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace LockIt
{
	public class KeyData
	{
		public enum Statuses
		{
			Present,
			Absent
		}

		public string Name { get; private set; }
		public string UUID { get; private set; }

		public Dictionary<string, string> Info { get; private set; }
		public List<AuditStamp> AuditTrail { get; private set; }

		public UserData LastUser { get; set; }
		public DateTime LastLoginTime { get; set; }
		public Statuses Status { get; set; }

		public List<UserData> UserList { get; set; }

		public KeyData(string name, string uuid, bool automotive)
		{
			Name = name;
			UUID = uuid;

			Info = new Dictionary<string, string>();

			if( automotive )
			{
				Info.Add( "VIN Nº", "1GTEK19B37Z663465" );
				Info.Add( "Stock Nº", "987654321000" );
				Info.Add( "Rego Nº", "ABC123" );
				Info.Add( "Make", "Toyota" );
				Info.Add( "Model", "Corolla" );
				Info.Add( "Colour", "White" );
			}
			else
			{
				Info.Add( "Client Code", "987654321000" );
				Info.Add( "Address Line 1", "123 Fake St" );
				Info.Add( "Address Line 2", "Unit 10" );
				Info.Add( "Suburb", "North Ryde" );
				Info.Add( "State", "NSW" );
				Info.Add( "Post Code", "2113" );
				Info.Add( "Large Key", "Front Door" );
				Info.Add( "Small Key", "Windows" );
				Info.Add( "Security Key", "Garage" );
			}

			AuditTrail = new List<AuditStamp>();

			AuditTrail.Add( new AuditStamp() { TimeStamp = DateTime.Now.Subtract(new TimeSpan(14, 2, 1, 0)), Name = "admin", Description = "Taken" } );
			AuditTrail.Add( new AuditStamp() { TimeStamp = DateTime.Now.Subtract(new TimeSpan(14, 1, 17, 0)), Name = "admin", Description = "Returned" } );
			AuditTrail.Add( new AuditStamp() { TimeStamp = DateTime.Now.Subtract(new TimeSpan(12, 12, 15, 0)), Name = "sam", Description = "Taken" } );
			AuditTrail.Add( new AuditStamp() { TimeStamp = DateTime.Now.Subtract(new TimeSpan(12, 8, 12, 0)), Name = "sam", Description = "Returned" } );
			AuditTrail.Add( new AuditStamp() { TimeStamp = DateTime.Now.Subtract(new TimeSpan(8, 0, 12, 0)), Name = "admin", Description = "Taken" } );
			AuditTrail.Add( new AuditStamp() { TimeStamp = DateTime.Now.Subtract(new TimeSpan(8, 0, 1, 0)), Name = "admin", Description = "Returned" } );
			AuditTrail.Add( new AuditStamp() { TimeStamp = DateTime.Now.Subtract(new TimeSpan(3, 7, 26, 0)), Name = "john", Description = "Taken" } );
			AuditTrail.Add( new AuditStamp() { TimeStamp = DateTime.Now.Subtract(new TimeSpan(3, 7, 7, 0)), Name = "john", Description = "Returned" } );
			AuditTrail.Add( new AuditStamp() { TimeStamp = DateTime.Now.Subtract(new TimeSpan(1, 2, 51, 0)), Name = "admin", Description = "Taken" } );
			AuditTrail.Add( new AuditStamp() { TimeStamp = DateTime.Now.Subtract(new TimeSpan(1, 1, 14, 0)), Name = "admin", Description = "Returned" } );
		}

		public bool IsAllowed(UserData user)
		{
			return UserList.Contains(user);
		}

		public string GetSearchString()
		{
			var retStr = Name + " " + UUID + " " + LastUser.Name;

			foreach( var key in Info.Keys.ToArray() )
			{
				retStr += " " + key + " " + Info[key];
			}

			return retStr;
		}
	}
}

