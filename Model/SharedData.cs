﻿using System;
using System.Collections.Generic;
using Foundation;
using UIKit;

namespace LockIt
{
	public class SharedData
	{
		public UserData LoggedInUser { get; set; }

		public List<UserData> UserList;
		public List<CabinetData> CabinetList;
		public List<PanelData> PanelList;

		public static UIColor TintColor { get { return UIColor.FromRGB(213/255f, 29/255f, 40/255f); } }
		public static UIColor BackgroundColor { get { return UIColor.FromRGB(221/255f, 230/255f, 238/255f); } }
		public static UIColor GreenTintColor { get { return UIColor.FromRGB(70/255f, 137/255f, 102/255f); } }

		#region Singleton Instance
		private static SharedData m_Instance;
		public static SharedData Instance
		{
			get
			{
				if( m_Instance == null )
				{
					m_Instance = new SharedData();
				}
				return m_Instance;
			}
		}
		#endregion Singeton Instance

		private SharedData()
		{
			UserList = new List<UserData>();
			CabinetList = new List<CabinetData>();
			PanelList = new List<PanelData>();

			UserList.Add( new UserData("admin", "abcd1234", UserData.AccessLevels.SystemManager) );
			UserList.Add( new UserData("samwilliams", "abcd1234", UserData.AccessLevels.Staff) );
			UserList.Add( new UserData("johnsmith", "abcd1234", UserData.AccessLevels.Staff) );

			string[] names = new string[] { "Automotive", "Real Estate" };

			CabinetList.Add( new CabinetData(names[0] + " Cabinet", "01234567890AA") { LastUser = UserList[0], LastLoginTime = DateTime.Now.Subtract( new TimeSpan(1, 1, 0) ), UserList = new List<UserData>() {UserList[0]} } );
			CabinetList.Add( new CabinetData(names[1] + " Cabinet", "01234567890EE") { LastUser = UserList[2], LastLoginTime = DateTime.Now.Subtract( new TimeSpan(3, 34, 0) ), UserList = new List<UserData>() {UserList[0], UserList[1], UserList[2]} } );

			for( int c = 0; c < CabinetList.Count; c++ )
			{
				for( int p = 0; p < 3; p++ )
				{
					CabinetList[c].AssignPanel( new PanelData(names[c] + " Panel " + (p+1), "00AAABCDEF" + (c+1) + (p+1)) { LastUser = UserList[0], LastLoginTime = DateTime.Now.Subtract( new TimeSpan(c, p, 0) ) } );

					if( c == 1 && p == 2 )
					{
						CabinetList[c].PanelList[p].Status = PanelData.Statuses.Absent;
						CabinetList[c].PanelList[p].LastUser = UserList[2];
						CabinetList[c].PanelList[p].LastLoginTime = DateTime.Now.Subtract( new TimeSpan(3, 33, 0) );
						CabinetList[c].PanelList[p].UserList = new List<UserData>() { UserList[0], UserList[2] };
						PanelList.Add(CabinetList[c].PanelList[p]);
					}

					for( int k = 0; k < 6; k++ )
					{
						CabinetList[c].PanelList[p].AssignKey( new KeyData(names[c] + " Key " + (p+1) + " - " + (k+1), "00FEDCBAA" + (c+1) + (p+1) + (k+1), c == 0) { LastUser = UserList[0], LastLoginTime = DateTime.Now.Subtract( new TimeSpan(c, p, 0) ) } );
						CabinetList[c].PanelList[p].KeyList[k].AuditTrail.Add( new AuditStamp() { TimeStamp = DateTime.Now.Subtract(new TimeSpan(1, 1, 0)), Name = "Admin", Description = "Taken" } );

						if( c == 1 && p == 2 )
						{
							CabinetList[c].PanelList[p].KeyList[k].Status = KeyData.Statuses.Absent;
							CabinetList[c].PanelList[p].KeyList[k].LastUser = UserList[2];
							//PanelList.Add(CabinetList[c].PanelList[p]);
						}
					}
				}
			}
		}
	}
}

