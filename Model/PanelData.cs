﻿using System;
using System.Collections.Generic;

namespace LockIt
{
	public class PanelData
	{
		public enum Statuses
		{
			Present,
			Absent
		}

		public string Name { get; private set; }
		public string UUID { get; private set; }

		public UserData LastUser { get; set; }
		public DateTime LastLoginTime { get; set; }
		public Statuses Status { get; set; }

		public List<UserData> UserList { get; set; }

		public List<KeyData> KeyList { get; private set; }

		public PanelData(string name, string uuid)
		{
			Name = name;
			UUID = uuid;

			KeyList = new List<KeyData>();
		}

		public void AssignKey(KeyData key)
		{
			key.UserList = UserList;
			KeyList.Add(key);
		}

		public bool IsAllowed(UserData user)
		{
			return UserList.Contains(user);
		}
	}
}

