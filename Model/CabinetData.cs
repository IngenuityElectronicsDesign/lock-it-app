﻿using System;
using System.Collections.Generic;

namespace LockIt
{
	public class CabinetData
	{
		public enum Statuses
		{
			Locked,
			Unlocked
		}

		public string Name { get; private set; }
		public string UUID { get; private set; }

		public UserData LastUser { get; set; }
		public DateTime LastLoginTime { get; set; }
		public Statuses Status { get; set; }

		public List<UserData> UserList { get; set; }
		public List<PanelData> PanelList { get; private set; }
		public List<KeyData> KeyList
		{
			get
			{
				List<KeyData> retList = new List<KeyData>();

				foreach( var panel in PanelList )
				{
					foreach( var key in panel.KeyList )
					{
						retList.Add(key);
					}
				}
				return retList;
			}
		}

		public CabinetData(string name, string uuid)
		{
			Name = name;
			UUID = uuid;

			Status = Statuses.Locked;

			UserList = new List<UserData>();
			PanelList = new List<PanelData>();
		}

		public void AssignPanel(PanelData panel)
		{
			panel.UserList = UserList;	
			PanelList.Add(panel);
		}

		public void AssignUser(UserData user)
		{
			UserList.Add(user);
		}

		public bool IsAllowed(UserData user)
		{
			return UserList.Contains(user);
		}
	}
}

