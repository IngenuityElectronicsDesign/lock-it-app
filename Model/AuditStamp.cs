﻿using System;

namespace LockIt
{
	public class AuditStamp
	{
		public DateTime TimeStamp { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }

		public AuditStamp()
		{
		}
	}
}

