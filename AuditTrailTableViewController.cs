using System;
using System.Collections.Generic;
using System.Linq;
using System.Timers;
using Foundation;
using UIKit;
using CoreGraphics;
using MessageUI;

namespace LockIt
{
	public partial class AuditTrailTableViewController : UITableViewController
	{
		public List<AuditStamp> Data;

		public AuditTrailTableViewController(IntPtr handle) : base(handle)
		{
		}

		public void Setup(string title, List<AuditStamp> data)
		{
			Title = title;
			Data = data;
		}

		#region View Controller Logic
		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			if( NavigationItem != null )
			{
				NavigationItem.RightBarButtonItem = new UIBarButtonItem(UIBarButtonSystemItem.Action, (sender, e) =>
					{
						UIAlertController alert = UIAlertController.Create("Audit Trail Logs", "", UIAlertControllerStyle.ActionSheet);
						alert.AddAction( UIAlertAction.Create("Email Logs", UIAlertActionStyle.Default, (alertAction) =>
							{
								MFMailComposeViewController mail = new MFMailComposeViewController();
								mail.SetSubject("Audit Trail Logs");
								mail.Finished += (s, ev) => { mail.DismissViewController(true, null); };
								PresentViewController(mail, true, null);
							}));
						alert.AddAction( UIAlertAction.Create("Clear All Logs", UIAlertActionStyle.Destructive, (alertAction) =>
							{
								UIAlertController clearAlert = UIAlertController.Create("Clear all audit trail history?", "This cannot be undone", UIAlertControllerStyle.Alert);
								clearAlert.AddAction( UIAlertAction.Create("Clear All", UIAlertActionStyle.Destructive, (clearAlertAction) => { Data.Clear(); TableView.ReloadSections(new NSIndexSet(0), UITableViewRowAnimation.Fade); } ));
								clearAlert.AddAction( UIAlertAction.Create("Cancel", UIAlertActionStyle.Cancel, null ));
								PresentViewController(clearAlert, true, null);
							}));
						alert.AddAction( UIAlertAction.Create("Cancel", UIAlertActionStyle.Cancel, null ));
						PresentViewController(alert, true, null);
					});
			}

			TableView.RegisterClassForCellReuse( typeof(KeyDetailCell), new NSString("DetailCell") );

			TableView.EstimatedRowHeight = 50;
			TableView.RowHeight = UITableView.AutomaticDimension;

			TableView.SeparatorStyle = UITableViewCellSeparatorStyle.None;
			TableView.SeparatorColor = UIColor.Clear;
		}

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);
		}

		public override void ViewDidDisappear(bool animated)
		{
			base.ViewDidDisappear(animated);
		}
		#endregion View Controller Logic

		#region Table View Logic
		public override nint NumberOfSections(UITableView tableView)
		{
			return 1;
		}

		public override nint RowsInSection(UITableView tableview, nint section)
		{
			return Data.Count;
		}

		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
			var data = Data[indexPath.Row];

			KeyDetailCell cell = (KeyDetailCell)TableView.DequeueReusableCell(new NSString("DetailCell"), indexPath);
			cell.Setup(data.TimeStamp.ToString("dd/MM  hh:mm"), ( Title.Contains("Cabinet") ? "Accessed" : data.Description ) + " by " + data.Name, false, false);
			return cell;
		}
		#endregion Table View Logic
	}
}
