// LockIt project - high level protocol definitions
// By Ran Peleg, Ingenuity
// Date: September, 2015
// ----------------------------------------------------------------------------

#ifndef _LOCKIT_PROTOCOL_DEFS_H_
#define _LOCKIT_PROTOCOL_DEFS_H_

#include "Common/CommonTypes.h"


#undef PACKED_STRUCT

// Align all structures to byte boundry
#ifdef __GNUC__
// if GCC or LLVM compilers
#  define PACKED_STRUCT __attribute__ ((packed))
#else
// If microsoft or IAR compilers
#  pragma pack(push,1)
#  define PACKED_STRUCT
#  define ALIGN_POP_IS_REQUIRED
#endif

#define MAX_BLE_PACKET_LENGTH 20

// Generic protocol header
struct TLockItProtocolHeader {
  U8 Cmd;
};

// Host to device messages
// ------------------------------------------------------------------

// Admin cabinet unlock command
#define LOCKIT_CMD_ADMIN_CABINET_UNLOCK 1

// Check if cabinet door is open
#define LOCKIT_CMD_CABINET_IS_DOOR_OPEN 2

// Device to host messages
// ------------------------------------------------------------------

// Logical Ack reply
#define LOCKIT_REPLY_ACK 1

// Logical Nack reply
#define LOCKIT_REPLY_NACK 2

// Cabinet door open state reply
#define LOCKIT_REPLY_CABINET_IS_DOOR_OPEN 3

struct TLockItReplyCabinetIsDoorOpen {
  struct TLockItProtocolHeader Header;
  U8 OpenStatus; // 0 = close, 1 = open
} PACKED_STRUCT;

// Restore previous align settings
#ifdef ALIGN_POP_IS_REQUIRED
#  pragma pack(pop)
#endif

#endif
