﻿using System;
using System.Collections.Generic;
using Foundation;
using UIKit;
using CoreGraphics;
using System.Timers;

namespace LockIt
{
	public class UnlockCell : UITableViewCell
	{
		public enum UnlockStates
		{
			Locked,
			Slider,
			Searching,
			Connecting,
			MoveCloser,
			Unlocking,
			Unlocked
		}

		private UnlockStates _UnlockState;
		public UnlockStates UnlockState
		{
			get { return _UnlockState; }
			set
			{
				InvokeOnMainThread (() => {
					UIView.Animate (0.6, 0, UIViewAnimationOptions.AllowUserInteraction, () => {
						switch (value) {
						case UnlockStates.Locked:
							SetCellUI (1, 0, UIColor.White, LockedTitle, SharedData.TintColor, SharedData.TintColor, UIImage.FromFile ("BigLock.png").ImageWithRenderingMode (UIImageRenderingMode.AlwaysTemplate));
							m_Data.Status = CabinetData.Statuses.Locked;
							break;
						case UnlockStates.Slider:
							Label.Alpha = 0;
							AirUnlockSlider.Alpha = 1;
							break;
						case UnlockStates.Searching:
							SetCellUI (1, 0, SharedData.TintColor, SearchingTitle, UIColor.White, UIColor.White, null);
							BLEInterface.Instance.ScanForPeripherals();
							break;
						case UnlockStates.Connecting:
							SetCellUI (1, 0, SharedData.TintColor, ConnectingTitle, UIColor.White, UIColor.White, null);
							break;
						case UnlockStates.MoveCloser:
							SetCellUI (1, 0, SharedData.TintColor, MoveCloserTitle, UIColor.White, UIColor.White, null);
							BLEInterface.Instance.RequestRSSIUpdates();
							break;
						case UnlockStates.Unlocking:
							SetCellUI (1, 0, SharedData.TintColor, UnlockingTitle, UIColor.White, UIColor.White, null);
							StartUnlockStatusPolling();
							break;
						case UnlockStates.Unlocked:
							SetCellUI (1, 0, SharedData.TintColor, UnlockedTitle, UIColor.White, UIColor.White, UIImage.FromFile ("BigLockOpen.png").ImageWithRenderingMode (UIImageRenderingMode.AlwaysTemplate));
							m_Data.Status = CabinetData.Statuses.Unlocked;
							break;
						}
					}, null);
				});
				
				_UnlockState = value;
			}
		}

		private void SetCellUI(nfloat labelAlpha, nfloat sliderAlpha, UIColor containerBgColor, string labelText, UIColor labelColor, UIColor accessoryColor, UIImage accessoryImage) {
			Label.Alpha = labelAlpha;
			AirUnlockSlider.Alpha = sliderAlpha;

			ContainerView.BackgroundColor = containerBgColor;//(value == UnlockStates.Locked ? UIColor.White : SharedData.TintColor);
			Label.Text = labelText;//(value == UnlockStates.Locked ? LockedTitle : UnlockedTitle);
			Label.TextColor = labelColor;//(value == UnlockStates.Locked ? SharedData.TintColor : UIColor.White);
			AccessoryImage.TintColor = accessoryColor;//(value == UnlockStates.Locked ? SharedData.TintColor : UIColor.White);
			if (accessoryImage == null) {
				AccessoryImage.Alpha = 0;
				ActivityIndicator.Alpha = 1;
				ActivityIndicator.StartAnimating ();
			} else {
				AccessoryImage.Alpha = 1;
				ActivityIndicator.Alpha = 0;
				ActivityIndicator.StopAnimating ();
				AccessoryImage.Image = accessoryImage;//(value == UnlockStates.Locked ? UIImage.FromFile("BigLock.png") : UIImage.FromFile("BigLockOpen.png")).ImageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate);
			}
		}

		UILabel Label;
		UIImageView AccessoryImage;
		UIActivityIndicatorView ActivityIndicator;

		PatternUnlockSliderView AirUnlockSlider;

		UIView ContainerView;

		string LockedTitle;
		string UnlockedTitle;
		string SearchingTitle;
		string ConnectingTitle;
		string UnlockingTitle;
		string MoveCloserTitle;

		Action UnlockAction;

		private Timer m_PollTimer = null;
		private int m_PollCount = 0;
		private bool m_RequestedUnlock = false;

		CabinetData m_Data;

		private float m_RSSIThreshold = -20f;

		public UnlockCell(IntPtr handle) : base(handle)
		{
			SelectionStyle = UITableViewCellSelectionStyle.None;
			BackgroundColor = UIColor.Clear;

			ContainerView = new UIView (CGRect.Empty);
			ContainerView.Layer.CornerRadius = 5;
			ContainerView.Layer.MasksToBounds = true;
			AddSubview (ContainerView);

			Label = new UILabel (CGRect.Empty);
			Label.Font = UIFont.PreferredHeadline;
			Label.AdjustsFontSizeToFitWidth = true;
			Label.Text = "Title";
			Label.MinimumScaleFactor = 0.01f;
			ContainerView.AddSubview (Label);

			AccessoryImage = new UIImageView (CGRect.Empty);
			ContainerView.AddSubview (AccessoryImage);

			ActivityIndicator = new UIActivityIndicatorView (CGRect.Empty);
			ActivityIndicator.ActivityIndicatorViewStyle = UIActivityIndicatorViewStyle.White;
			ActivityIndicator.Layer.CornerRadius = 5;
			ActivityIndicator.Layer.MasksToBounds = true;
			AddSubview (ActivityIndicator);

			PatternUnlockPattern Pattern = new PatternUnlockPattern (UIImage.FromFile ("AirUnlock/pattern-locked.png").ImageWithAlignmentRectInsets (new UIEdgeInsets (0, 80, 0, 80)), UIImage.FromFile ("AirUnlock/pattern-locked.png").ImageWithAlignmentRectInsets (new UIEdgeInsets (0, 80, 0, 80)), new List<CGPoint> (new CGPoint[] {
				new CGPoint (0f, 0f),
				new CGPoint (240f, 0f)
			}));

			AirUnlockSlider = new PatternUnlockSliderView (new CGRect (40, 5, 280, 40), Pattern, () => { // Completion
				UnlockState = UnlockStates.Searching;
				//UnlockState = UnlockStates.Unlocked;
				UnlockAction ();
			}, () => { // Timeout
				UnlockState = UnlockStates.Locked;
			});
			AirUnlockSlider.Alpha = 0;
			ContainerView.AddSubview (AirUnlockSlider);

			BLEInterface.Instance.Connected += BLEInterface_Instance_Connected;
			BLEInterface.Instance.PeripheralDiscovered += BLEInterface_Instance_PeripheralDiscovered;
			BLEInterface.Instance.ConnectedPeripheralRSSIUpdated += BLEInterface_Instance_ConnectedPeripheralRSSIUpdated;
			BLEInterface.Instance.CabinetDoorIsOpenResponseReceived += BLEInterface_Instance_CabinetDoorIsOpenResponseReceived;
			// TODO : These event handlers could cause memory leaks.  Need to properly clean up.  
		}

		void BLEInterface_Instance_ConnectedPeripheralRSSIUpdated (float rssi)
		{
			if (UnlockState == UnlockStates.MoveCloser && rssi > m_RSSIThreshold) {
				UnlockState = UnlockStates.Unlocking;
			}
		}

		void BLEInterface_Instance_CabinetDoorIsOpenResponseReceived (BLEInterface.TLockItReplyCabinetIsDoorOpen isDoorOpen)
		{
			if (isDoorOpen.OpenStatus == (byte)BLEInterface.LockitCabinetOpenStatus.Open) {
				UnlockState = UnlockStates.Unlocked;
				CleanUnlockStatusPolling ();
				BLEInterface.Instance.DisconnectFromCurrentPeripheral ();
			}
		}

		void BLEInterface_Instance_PeripheralDiscovered (string name)
		{
			if (name == "lockit_cabinet") {
				// This is our peripheral, connect to it.
				if (!BLEInterface.Instance.ConnectToPeripheral (name)) {
					// Error 
					UnlockState = UnlockStates.Locked;
				} else {
					UnlockState = UnlockStates.Connecting;
				}
			}
		}

		void BLEInterface_Instance_Connected ()
		{
			if (UnlockState == UnlockStates.Connecting) {
				UnlockState = UnlockStates.Unlocking;
			}
		}

		private void StartUnlockStatusPolling() {
			
			if (m_PollTimer == null) {
				m_PollCount = 0;
				m_PollTimer = new Timer (1000);
				m_PollTimer.Elapsed += delegate {
					if(m_PollTimer != null && BLEInterface.Instance.IsReadyToSend) {
						if(!m_RequestedUnlock) {
							BLEInterface.Instance.RequestCanibetUnlock();
							m_RequestedUnlock = true;
						} else if(m_PollCount < 20) {
							BLEInterface.Instance.RequestCabinetIsDoorOpen();
							m_PollCount++;
						} else {
							// Timeout
							UnlockState = UnlockStates.Locked;
							CleanUnlockStatusPolling();
						}
					}
				};
				m_PollTimer.Start ();
			}
		}

		private void CleanUnlockStatusPolling() {
			if (m_PollTimer != null) {
				m_PollTimer.Stop ();
				m_PollTimer = null;
			}
		}

		CabinetsDetailTableViewController m_DetailViewController;

		public void Setup(CabinetsDetailTableViewController detailViewController, CabinetData data, string lockedTitle, string unlockedTitle, string searchingTitle, string connectingTitle, string unlockingTitle, string moveCloserTitle,  Action unlockAction)
		{
			m_DetailViewController = detailViewController;
			m_Data = data;
			LockedTitle = lockedTitle;
			UnlockedTitle = unlockedTitle;
			SearchingTitle = searchingTitle;
			ConnectingTitle = connectingTitle;
			UnlockingTitle = unlockingTitle;
			MoveCloserTitle = moveCloserTitle;

			UnlockState = (data.Status == CabinetData.Statuses.Unlocked ? UnlockStates.Unlocked : UnlockStates.Locked);
			UnlockAction = unlockAction;
		}

		public void CellPressed()
		{
			if (UnlockState == UnlockStates.Locked) {
				UnlockState = UnlockStates.Slider;
			} else if (UnlockState == UnlockStates.Searching) {
				UnlockState = UnlockStates.Locked;
			}
		}

		public override CGSize SizeThatFits(CGSize size)
		{
			size.Height = 60;

			ContainerView.Frame = new CGRect(10, 0, size.Width-20, size.Height-10);

			Label.Frame = new CGRect(40, 15, size.Width-70, 20);
			AccessoryImage.Frame = new CGRect(10, 15, 20, 20);
			ActivityIndicator.Frame = new CGRect(20, 15, 20, 20);

			AirUnlockSlider.Frame = new CGRect((size.Width-300)/2, 5, 280, 40);

			return size;
		}

		public override void SetSelected(bool selected, bool animated)
		{
			base.SetSelected(false, true);
		}
	}
}
