﻿using System;
using Foundation;
using UIKit;
using CoreGraphics;

namespace LockIt
{
	public class CabinetSummaryCell : UITableViewCell
	{
		public CabinetData Data { get; private set; }

		UIView ContainerView;
		UIView TintView;
		UIView SeparatorLineView;

		UIImageView StatusImageView;

		UILabel NameLabel;
		UILabel UUIDLabel;
		UILabel LastUsedNameLabel;
		UILabel LastUsedTimeLabel;

		UIImageView DisclosureImage;

		public CabinetSummaryCell(IntPtr handle) : base(handle)
		{
			SelectionStyle = UITableViewCellSelectionStyle.None;
			BackgroundColor = UIColor.Clear;

			ContainerView = new UIView(CGRect.Empty);
			ContainerView.BackgroundColor = UIColor.White;
			ContainerView.Layer.CornerRadius = 5;
			ContainerView.Layer.MasksToBounds = true;
			AddSubview(ContainerView);

			TintView = new UIView(CGRect.Empty);
			ContainerView.AddSubview(TintView);

			SeparatorLineView = new UIView(CGRect.Empty);
			SeparatorLineView.BackgroundColor = UIColor.FromWhiteAlpha(0.2f, 0.2f);
			ContainerView.AddSubview(SeparatorLineView);

			StatusImageView = new UIImageView(CGRect.Empty);
			ContainerView.AddSubview(StatusImageView);

			NameLabel = new UILabel(CGRect.Empty);
			NameLabel.Font = UIFont.PreferredHeadline;
			NameLabel.Text = "Cabinet Name";
			ContainerView.AddSubview(NameLabel);

			UUIDLabel = new UILabel(CGRect.Empty);
			UUIDLabel.Font = UIFont.PreferredSubheadline;
			UUIDLabel.Text = "UUID";
			ContainerView.AddSubview(UUIDLabel);

			LastUsedNameLabel = new UILabel(CGRect.Empty);
			LastUsedNameLabel.Font = UIFont.PreferredCaption2;
			LastUsedNameLabel.Text = "User";
			LastUsedNameLabel.TextAlignment = UITextAlignment.Center;
			ContainerView.AddSubview(LastUsedNameLabel);

			LastUsedTimeLabel = new UILabel(CGRect.Empty);
			LastUsedTimeLabel.Font = UIFont.PreferredCaption1;
			LastUsedTimeLabel.Text = "31/12/15 11:59 PM";
			LastUsedTimeLabel.TextAlignment = UITextAlignment.Center;
			ContainerView.AddSubview(LastUsedTimeLabel);

			DisclosureImage = new UIImageView(CGRect.Empty);
			DisclosureImage.Image = UIImage.FromFile("Disclosure.png").ImageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate);
			ContainerView.AddSubview(DisclosureImage);
		}

		public void Setup(CabinetData data)
		{
			Data = data;

			NameLabel.Text = Data.Name;
			UUIDLabel.Text = Data.UUID;

			if( Data.LastUser != null )
			{
				LastUsedNameLabel.Text = (Data.LastUser.Name == SharedData.Instance.LoggedInUser.Name ? "You" : Data.LastUser.Name);	
			}
		
			LastUsedTimeLabel.Text = Data.LastLoginTime.ToShortTimeString();	
		}

		public override CGSize SizeThatFits(CGSize size)
		{
			size.Height = 90;

			Position(size);

			return size;
		}

		public void Position(CGSize size)
		{
			ContainerView.Frame = new CGRect(10, 0, size.Width-20, size.Height-10);

			if( Data.Status == CabinetData.Statuses.Locked )
			{
				TintView.Frame = new CGRect(0, 0, 6, ContainerView.Bounds.Height);
				TintView.BackgroundColor = SharedData.TintColor; //GreenTintColor;

				StatusImageView.Image = UIImage.FromFile("BigLock.png").ImageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate);
				StatusImageView.TintColor = UIColor.Black;
				LastUsedNameLabel.TextColor = UIColor.Black;
				LastUsedTimeLabel.TextColor = UIColor.Black;
				SeparatorLineView.Hidden = false;

				NameLabel.TextColor = SharedData.TintColor; //GreenTintColor;
				DisclosureImage.TintColor = SharedData.TintColor; //GreenTintColor;
			}
			if( Data.Status == CabinetData.Statuses.Unlocked )
			{
				TintView.Frame = new CGRect(0, 0, ContainerView.Bounds.Width*0.3, ContainerView.Bounds.Height);
				TintView.BackgroundColor = SharedData.TintColor;

				StatusImageView.Image = UIImage.FromFile("BigLockOpen.png").ImageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate);
				StatusImageView.TintColor = UIColor.White;
				LastUsedNameLabel.TextColor = UIColor.FromWhiteAlpha(1, 0.8f);
				LastUsedTimeLabel.TextColor = UIColor.White;
				SeparatorLineView.Hidden = true;

				NameLabel.TextColor = SharedData.TintColor;
				DisclosureImage.TintColor = SharedData.TintColor;
			}

			StatusImageView.Frame = new CGRect(ContainerView.Bounds.Width*0.15-12.5, 10, 25, 25);
			SeparatorLineView.Frame = new CGRect(ContainerView.Bounds.Width*0.3, 5, 1, ContainerView.Bounds.Height-10);

			NameLabel.Frame = new CGRect(ContainerView.Bounds.Width*0.3+10, 10f, ContainerView.Bounds.Width*0.7-40, 20f);
			UUIDLabel.Frame = new CGRect(ContainerView.Bounds.Width*0.3+10, 40f, ContainerView.Bounds.Width*0.7-40, 20f);

			LastUsedNameLabel.Frame = new CGRect(ContainerView.Bounds.Width*0.025, 40f, ContainerView.Bounds.Width*0.25, 15f);
			LastUsedTimeLabel.Frame = new CGRect(ContainerView.Bounds.Width*0.025, 55f, ContainerView.Bounds.Width*0.25, 15f);
			DisclosureImage.Frame = new CGRect(ContainerView.Bounds.Width-25, 10, 20, 20);
		}

		public override void SetSelected(bool selected, bool animated)
		{
			base.SetSelected(selected, animated);

			if( animated )
			{
				UIView.Animate(0.6, 0, UIViewAnimationOptions.AllowUserInteraction, () =>
					{
						ContainerView.Alpha = selected ? 0.8f : 1;
					}, null);
			}
			else
			{
				ContainerView.Alpha = selected ? 0.8f : 1;
			}
		}
	}
}
