﻿using System;
using Foundation;
using UIKit;
using CoreGraphics;

namespace LockIt
{
	public class ButtonCell : UITableViewCell
	{
		UILabel Label;

		UIView ContainerView;

		UIImageView AccessoryImage;
		UIImageView DisclosureImage;

		public ButtonCell(IntPtr handle) : base(handle)
		{
			SelectionStyle = UITableViewCellSelectionStyle.None;
			BackgroundColor = UIColor.Clear;

			ContainerView = new UIView(CGRect.Empty);
			ContainerView.BackgroundColor = UIColor.White;
			ContainerView.Layer.CornerRadius = 5;
			ContainerView.Layer.MasksToBounds = true;
			AddSubview(ContainerView);

			Label = new UILabel(CGRect.Empty);
			Label.Font = UIFont.PreferredHeadline;
			Label.Text = "Title";
			Label.TextColor = SharedData.TintColor;
			ContainerView.AddSubview(Label);

			AccessoryImage = new UIImageView(CGRect.Empty);
			AccessoryImage.TintColor = SharedData.TintColor;
			ContainerView.AddSubview(AccessoryImage);

			DisclosureImage = new UIImageView(CGRect.Empty);
			DisclosureImage.Image = UIImage.FromFile("Disclosure.png").ImageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate);
			DisclosureImage.TintColor = SharedData.TintColor;
			ContainerView.AddSubview(DisclosureImage);
		}

		public void Setup(string title, UIImage accessoryImage)
		{
			Label.Text = title;
			AccessoryImage.Image = accessoryImage.ImageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate);
		}

		public override CGSize SizeThatFits(CGSize size)
		{
			size.Height = 50;

			ContainerView.Frame = new CGRect(10, 0, size.Width-20, size.Height-10);

			Label.Frame = new CGRect(40, 10, size.Width-85, 20);
			AccessoryImage.Frame = new CGRect(10, 10, 20, 20);
			DisclosureImage.Frame = new CGRect(size.Width-45, 10, 20, 20);

			return size;
		}

		public override void SetSelected(bool selected, bool animated)
		{
			base.SetSelected(false, true);
		}
	}
}
