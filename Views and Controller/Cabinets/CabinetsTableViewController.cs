using System;
using System.Collections.Generic;
using System.Timers;
using Foundation;
using UIKit;
using CoreGraphics;

namespace LockIt
{
	public partial class CabinetsTableViewController : UITableViewController
	{
		List<CabinetData> CabinetList;

		public CabinetsTableViewController(IntPtr handle) : base(handle)
		{
		}

		#region View Controller Logic
		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			CabinetList = new List<CabinetData>();

			NavigationController.NavigationBar.BarTintColor = SharedData.BackgroundColor;

			if( NavigationItem != null )
			{
				NavigationItem.LeftBarButtonItem = new UIBarButtonItem(UIImage.FromFile("LogOut.png"), UIBarButtonItemStyle.Plain, (sender, e) =>
					{
						DismissViewController(true, () => { SharedData.Instance.LoggedInUser = null; });	
					});
			}
		
			RefreshControl = new UIRefreshControl();
			RefreshControl.ValueChanged += (sender, e) => 
			{
				if( RefreshControl.Refreshing )
				{
					RefreshControl.BeginRefreshing();

					if(!BLEInterface.Instance.ScanForPeripherals()) {
						ShowBleNotOnAlert();
					} else {
						Timer timer = new Timer(1500);
						timer.Elapsed += (s, ev) => 
						{
							InvokeOnMainThread( () =>
								{
									// This is where it will talk to the server.
									RefreshCallback();

									timer.Stop();
									timer = null;
								});
						};
						timer.Start();
					}
				}
			};

			Timer firstTimer = new Timer(1500);
			firstTimer.Elapsed += (sender, e) => 
			{
				InvokeOnMainThread( () =>
					{
						RefreshCallback();

						firstTimer.Stop();
						firstTimer = null;
					});
			};
			firstTimer.Start();

			TableView.RegisterClassForCellReuse( typeof(CabinetSummaryCell), new NSString("SummaryCell") );

			TableView.EstimatedRowHeight = 80;
			TableView.RowHeight = UITableView.AutomaticDimension;

			TableView.SeparatorStyle = UITableViewCellSeparatorStyle.None;
			TableView.SeparatorColor = UIColor.Clear;
		}

		private void RefreshCallback()
		{
			CabinetList.Clear();

			foreach( var cabinet in SharedData.Instance.CabinetList )
			{
				if( cabinet.IsAllowed(SharedData.Instance.LoggedInUser) )
				{
					CabinetList.Add(cabinet);	
				}
			}

			if( RefreshControl != null && RefreshControl.Refreshing )
			{
				RefreshControl.EndRefreshing();
			}
			TableView.ReloadData();
		}

		bool first = true;

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);

			if (first) {
				TableView.ContentOffset = new CGPoint (0, TableView.ContentOffset.Y - RefreshControl.Frame.Height);
				RefreshControl.BeginRefreshing ();
				first = false;
			} else {
				TableView.ReloadSections (new NSIndexSet (0), UITableViewRowAnimation.Fade);
			}
		}

		public override void ViewDidDisappear(bool animated)
		{
			if( RefreshControl != null && RefreshControl.Refreshing )
			{
				RefreshControl.EndRefreshing();
			}

			base.ViewDidDisappear(animated);
		}
		#endregion View Controller Logic

		#region Table View Logic
		public override nint NumberOfSections(UITableView tableView)
		{
			return 1;
		}

		public override nint RowsInSection(UITableView tableview, nint section)
		{
			return CabinetList.Count;
		}

		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
			CabinetSummaryCell cell = (CabinetSummaryCell)TableView.DequeueReusableCell(new NSString("SummaryCell"), indexPath);
			cell.Setup(CabinetList[indexPath.Row]);
			return cell;
		}

		public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
		{
			CabinetsDetailTableViewController toPush = (CabinetsDetailTableViewController)Storyboard.InstantiateViewController("CabinetsDetailTVC");
			toPush.Setup(CabinetList[indexPath.Row]);
			NavigationController.PushViewController(toPush, true);
		}

		public override nfloat GetHeightForHeader(UITableView tableView, nint section)
		{
			return (CabinetList.Count == 0) ? 64 : 0;
		}

		public override UIView GetViewForHeader(UITableView tableView, nint section)
		{
			if( (CabinetList.Count == 0) )
			{
				UILabel headerView = new UILabel( new CGRect(0, 0, View.Bounds.Width, 64) );
				headerView.Font = UIFont.PreferredSubheadline;
				headerView.Text = "Searching for Cabinets...";
				headerView.TextAlignment = UITextAlignment.Center;
				headerView.TextColor = SharedData.TintColor;
				return headerView;
			}
			return null;
		}
		#endregion Table View Logic

		void ShowBleNotOnAlert ()
		{
			if (UIDevice.CurrentDevice.CheckSystemVersion (8, 0)) {
				var alert = UIAlertController.Create ("Bluetooth is Disabled", "Bluetooth is currently disabled.  Please go to device settings and turn it on.", UIAlertControllerStyle.Alert);
				alert.AddAction (UIAlertAction.Create ("OK", UIAlertActionStyle.Cancel, a =>  {
				}));
				PresentViewController (alert, true, null);
			}
			else {
				UIAlertView alert = new UIAlertView () {
					Title = "Bluetooth is Disabled",
					Message = "Bluetooth is currently disabled.  Please go to device settings and turn it on."
				};
				alert.AddButton ("OK");
				alert.AlertViewStyle = UIAlertViewStyle.Default;
				alert.Show ();
			}
		}
	}
}
