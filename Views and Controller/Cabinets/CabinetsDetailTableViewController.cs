using System;
using System.Collections.Generic;
using System.Timers;
using Foundation;
using UIKit;
using CoreGraphics;

namespace LockIt
{
	public partial class CabinetsDetailTableViewController : UITableViewController
	{
		CabinetData Data;

		public CabinetsDetailTableViewController (IntPtr handle) : base (handle)
		{
		}

		public void Setup(CabinetData data)
		{
			Data = data;

			Title = Data.Name;
		}

		#region View Controller Logic
		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			NavigationController.NavigationBar.BarTintColor = SharedData.BackgroundColor;

			TableView.RegisterClassForCellReuse( typeof(ButtonCell), new NSString("ButtonCell") );
			TableView.RegisterClassForCellReuse( typeof(UnlockCell), new NSString("UnlockCell") );

			TableView.EstimatedRowHeight = 50;
			TableView.RowHeight = UITableView.AutomaticDimension;

			TableView.SeparatorStyle = UITableViewCellSeparatorStyle.None;
			TableView.SeparatorColor = UIColor.Clear;
		}

		public override void ViewWillDisappear (bool animated)
		{
			base.ViewWillDisappear (animated);

			BLEInterface.Instance.DisconnectFromCurrentPeripheral ();
		}

		#endregion View Controller Logic

		#region Table View Logic
		public override nint NumberOfSections(UITableView tableView)
		{
			if( SharedData.Instance.LoggedInUser.AccessLevel == UserData.AccessLevels.SystemManager )
			{
				return 3;
			}
			return 2;
		}

		public override nint RowsInSection(UITableView tableview, nint section)
		{
			if( section == 2 )
			{
				return 1;
			}
			return 2;
		}

		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
			UITableViewCell c = base.GetCell(tableView, indexPath);

			if( indexPath.Section == 0 && indexPath.Row == 0 )
			{
				ButtonCell cell = (ButtonCell)TableView.DequeueReusableCell(new NSString("ButtonCell"), indexPath);
				cell.Setup("View Panel List", UIImage.FromFile("Panels.png"));
				c = cell;
			}
			else if( indexPath.Section == 0 && indexPath.Row == 1 )
			{
				ButtonCell cell = (ButtonCell)TableView.DequeueReusableCell(new NSString("ButtonCell"), indexPath);
				cell.Setup("View Key List", UIImage.FromFile("Key.png"));
				c = cell;
			}
			else if( indexPath.Section == 1 && indexPath.Row == 0 )
			{
				UnlockCell cell = (UnlockCell)TableView.DequeueReusableCell(new NSString("UnlockCell"), indexPath);
				cell.Setup(this, Data, "Unlock Cabinet", "Cabinet Unlocked", "Searching For Cabinet - Tap to Cancel", "Connecting to Cabinet", "Unlocking Cabinet", "Move closer to cabinet to Unlock", () => { });
				c = cell;
			}
			else if( indexPath.Section == 1 && indexPath.Row == 1 )
			{
				ButtonCell cell = (ButtonCell)TableView.DequeueReusableCell(new NSString("ButtonCell"), indexPath);
				cell.Setup("View Access History", UIImage.FromFile("History.png"));
				c = cell;
			}
			else if( indexPath.Section == 2 )
			{
				ButtonCell cell = (ButtonCell)TableView.DequeueReusableCell(new NSString("ButtonCell"), indexPath);
				cell.Setup("Assign Users", UIImage.FromFile("User.png"));
				c = cell;
			}

			return c;
		}

		public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
		{
			if( indexPath.Section == 0 && indexPath.Row == 0 )
			{
				SubPanelsTableViewController toPush = (SubPanelsTableViewController)Storyboard.InstantiateViewController("SubPanelsTVC");
				toPush.Setup(Data);
				NavigationController.PushViewController(toPush, true);
			}
			else if( indexPath.Section == 0 && indexPath.Row == 1 )
			{
				KeysTableViewController toPush = (KeysTableViewController)Storyboard.InstantiateViewController("KeysTVC");
				toPush.Setup(Data);
				NavigationController.PushViewController(toPush, true);
			}
			else if( indexPath.Section == 1 && indexPath.Row == 0 )
			{
				((UnlockCell)tableView.CellAt(indexPath)).CellPressed();
			}
			else if( indexPath.Section == 1 && indexPath.Row == 1 )
			{
				AuditTrailTableViewController toPush = (AuditTrailTableViewController)Storyboard.InstantiateViewController("AuditTrailTVC");
				toPush.Setup(Data.Name, Data.KeyList[0].AuditTrail);
				NavigationController.PushViewController(toPush, true);
			}
			else if( indexPath.Section == 2 )
			{
				List<UserData> otherUserList = new List<UserData>();

				foreach( var user in SharedData.Instance.UserList )
				{
					otherUserList.Add(user);

					foreach( var selUser in Data.UserList )
					{
						if( user.Name == selUser.Name )
						{
							otherUserList.Remove(user);
							break;
						}
					}
				}

				UserSubTableViewController toPush = new UserSubTableViewController(Data.UserList, otherUserList);
				toPush.View.BackgroundColor = View.BackgroundColor;
				NavigationController.PushViewController(toPush, true);
			}

//			CabinetsDetailTableViewController toPush = (CabinetsDetailTableViewController)Storyboard.InstantiateViewController("CabinetsDetailTVC");
//			toPush.Setup(CabinetList[indexPath.Row]);
//			NavigationController.PushViewController(toPush, true);
		}

//		public override nfloat GetHeightForHeader(UITableView tableView, nint section)
//		{
//			return (CabinetList.Count == 0) ? 64 : 0;
//		}
//
//		public override UIView GetViewForHeader(UITableView tableView, nint section)
//		{
//			if( (CabinetList.Count == 0) )
//			{
//				UILabel headerView = new UILabel( new CGRect(0, 0, View.Bounds.Width, 64) );
//				headerView.Font = UIFont.PreferredSubheadline;
//				headerView.Text = "Searching for Cabinets...";
//				headerView.TextAlignment = UITextAlignment.Center;
//				headerView.TextColor = SharedData.TintColor;
//				return headerView;
//			}
//			return null;
//		}
		#endregion Table View Logic
	}
}
