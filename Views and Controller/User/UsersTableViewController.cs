using System;
using System.Collections.Generic;
using System.Timers;
using Foundation;
using UIKit;
using CoreGraphics;

namespace LockIt
{
	public partial class UsersTableViewController : UITableViewController
	{
		List<UserData> UserList;

		public UsersTableViewController (IntPtr handle) : base (handle)
		{
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			UserList = new List<UserData>();

			foreach( var user in SharedData.Instance.UserList )
			{
				UserList.Add(user);
			}

			NavigationController.NavigationBar.BarTintColor = SharedData.BackgroundColor;

			if( NavigationItem != null )
			{
				NavigationItem.LeftBarButtonItem = new UIBarButtonItem(UIImage.FromFile("LogOut.png"), UIBarButtonItemStyle.Plain, (sender, e) =>
					{
						DismissViewController(true, () => { SharedData.Instance.LoggedInUser = null; });	
					});
				NavigationItem.RightBarButtonItem = new UIBarButtonItem(UIBarButtonSystemItem.Add, (sender, e) =>
					{
						if( UserList.Count == 3 )
						{
							SharedData.Instance.UserList.Add( new UserData("sarahconnor", "abcd1234", UserData.AccessLevels.StockManager) );
							UserList.Add( SharedData.Instance.UserList[3] );
							TableView.ReloadData();	
						}
						else if( UserList.Count == 4 )
						{
							SharedData.Instance.UserList.Add( new UserData("billanderson", "abcd1234", UserData.AccessLevels.Staff) );
							UserList.Add( SharedData.Instance.UserList[4] );
							TableView.ReloadData();	
						}
					});
			}
			TableView.RegisterClassForCellReuse( typeof(UserSummaryCell), new NSString("SummaryCell") );

			TableView.EstimatedRowHeight = 50;
			TableView.RowHeight = UITableView.AutomaticDimension;

			TableView.SeparatorStyle = UITableViewCellSeparatorStyle.None;
			TableView.SeparatorColor = UIColor.Clear;

			TableView.DataSource = new UsersTableViewDataSource(UserList);
		}
	}
}
