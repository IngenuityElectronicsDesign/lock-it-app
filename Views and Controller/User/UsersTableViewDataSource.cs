﻿using System;
using System.Collections.Generic;
using Foundation;
using UIKit;

namespace LockIt
{
	public class UsersTableViewDataSource : UITableViewDataSource
	{
		List<UserData> UserList;
		List<UserData> OtherUserList;

		public UsersTableViewDataSource( List<UserData> userList )
		{
			UserList = userList;
		}

		public UsersTableViewDataSource( List<UserData> userList, List<UserData> otherUserList ) : this( userList )
		{
			OtherUserList = otherUserList;
		}

		public override nint NumberOfSections(UITableView tableView)
		{
			return (OtherUserList != null && OtherUserList.Count > 0) ? 2 : 1;
		}

		public override nint RowsInSection(UITableView tableview, nint section)
		{
			if( section == 1 )
			{
				return OtherUserList.Count;
			}
			return UserList.Count;	
		}

		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
			UserSummaryCell cell = (UserSummaryCell)tableView.DequeueReusableCell(new NSString("SummaryCell"), indexPath);

			if( indexPath.Section == 0 )
			{
				cell.Setup(UserList[indexPath.Row]);
			}
			else if( indexPath.Section == 1 )
			{
				cell.Setup(OtherUserList[indexPath.Row]);
			}

			return cell;
		}

		public override string TitleForHeader(UITableView tableView, nint section)
		{
			if( OtherUserList != null && OtherUserList.Count > 0 )
			{
				return section == 0 ? "Assigned Users" : "Non-Assigned Users";
			}
			return "";
		}
	}
}

