﻿using System;
using Foundation;
using UIKit;
using CoreGraphics;

namespace LockIt
{
	public class UserSummaryCell : UITableViewCell
	{
		public UserData Data { get; private set; }

		UIView ContainerView;

		UILabel NameLabel;
		UILabel IsAdminLabel;

		UIImageView DisclosureImage;

		public UserSummaryCell(IntPtr handle) : base(handle)
		{
			SelectionStyle = UITableViewCellSelectionStyle.None;
			BackgroundColor = UIColor.Clear;

			ContainerView = new UIView(CGRect.Empty);
			ContainerView.BackgroundColor = UIColor.White;
			ContainerView.Layer.CornerRadius = 5;
			ContainerView.Layer.MasksToBounds = true;
			AddSubview(ContainerView);

			NameLabel = new UILabel(CGRect.Empty);
			NameLabel.Font = UIFont.PreferredHeadline;
			NameLabel.Text = "User Name";
			NameLabel.TextColor = SharedData.TintColor;
			ContainerView.AddSubview(NameLabel);

			IsAdminLabel = new UILabel(CGRect.Empty);
			IsAdminLabel.Font = UIFont.PreferredSubheadline;
			IsAdminLabel.Text = "Admin";
			IsAdminLabel.TextAlignment = UITextAlignment.Right;
			ContainerView.AddSubview(IsAdminLabel);

			DisclosureImage = new UIImageView(new CGRect(Bounds.Width-12, 12, 20, 20));
			DisclosureImage.Image = UIImage.FromFile("Disclosure.png").ImageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate);
			DisclosureImage.TintColor = SharedData.TintColor;
			ContainerView.AddSubview(DisclosureImage);
		}

		public void Setup(UserData data)
		{
			Data = data;

			NameLabel.Text = Data.Name;
			IsAdminLabel.Text = Data.AccessLevel == UserData.AccessLevels.SystemManager ? "System Manager" : Data.AccessLevel == UserData.AccessLevels.StockManager ? "Stock Manager" : "Staff";
		}

		public void Setup(UserData data, bool isSelected)
		{
			Setup(data);

			DisclosureImage.Image = UIImage.FromFile( isSelected ? "BigTick.png" : "BigExclaim.png" ).ImageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate);
		}

		public override CGSize SizeThatFits(CGSize size)
		{
			size.Height = 50;

			Position(size);

			return size;
		}

		public void Position(CGSize size)
		{
			ContainerView.Frame = new CGRect(10, 0, size.Width-20, size.Height-10);

			NameLabel.Frame = new CGRect(10f, 10f, ContainerView.Bounds.Width/2, 20f);
			IsAdminLabel.Frame = new CGRect(ContainerView.Bounds.Width/2, 10f, ContainerView.Bounds.Width/2-30f, 20f);
			DisclosureImage.Frame = new CGRect(ContainerView.Bounds.Width-25, 10, 20, 20);
		}

		public override void SetSelected(bool selected, bool animated)
		{
			base.SetSelected(false, animated);

			if( animated )
			{
				UIView.Animate(0.6, 0, UIViewAnimationOptions.AllowUserInteraction, () =>
					{
						ContainerView.Alpha = selected ? 0.8f : 1;
					}, null);
			}
			else
			{
				ContainerView.Alpha = selected ? 0.8f : 1;
			}
		}
	}
}
