﻿using System;
using System.Collections.Generic;
using System.Timers;
using Foundation;
using UIKit;
using CoreGraphics;

namespace LockIt
{
	public partial class UserSubTableViewController : UITableViewController
	{
		List<UserData> UserList;
		List<UserData> OtherUserList;

		public UserSubTableViewController(List<UserData> userList, List<UserData> otherUserList) : base(UITableViewStyle.Grouped)
		{
			UserList = userList;
			OtherUserList = otherUserList;
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			Title = "Assign Users";

			TableView.RegisterClassForCellReuse( typeof(UserSummaryCell), new NSString("SummaryCell") );

			TableView.EstimatedRowHeight = 50;
			TableView.RowHeight = UITableView.AutomaticDimension;

			TableView.SeparatorStyle = UITableViewCellSeparatorStyle.None;
			TableView.SeparatorColor = UIColor.Clear;

			TableView.DataSource = new UsersTableViewDataSource(UserList, OtherUserList);
		}

		public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
		{
			if( indexPath.Section == 0 && indexPath.Row != 0 )
			{
				OtherUserList.Add(UserList[indexPath.Row]);
				UserList.RemoveAt(indexPath.Row);
			}
			else
			{
				UserList.Add(OtherUserList[indexPath.Row]);
				OtherUserList.RemoveAt(indexPath.Row);
			}

			tableView.ReloadData();
		}
	}
}
