using System;

using Foundation;
using UIKit;

namespace LockIt
{
	public partial class LITabBarController : UITabBarController
	{
		public LITabBarController(IntPtr handle) : base(handle)
		{
		}

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);

			UIViewController cabinets = (UIViewController)Storyboard.InstantiateViewController("CabinetsTVC");
			UIViewController panels = (UIViewController)Storyboard.InstantiateViewController("PanelsTVC");
			UIViewController users = (UIViewController)Storyboard.InstantiateViewController("UsersTVC");

			ViewControllers = SharedData.Instance.LoggedInUser.AccessLevel == UserData.AccessLevels.SystemManager ? new UIViewController[] { cabinets, panels, users } : new UIViewController[] { cabinets, panels };

			cabinets.TabBarItem = new UITabBarItem("Cabinets", UIImage.FromFile("Cabinets.png"), UIImage.FromFile("CabinetsSelected.png"));
			panels.TabBarItem = new UITabBarItem("Panels", UIImage.FromFile("Panels.png"), UIImage.FromFile("PanelsSelected.png"));
			users.TabBarItem = new UITabBarItem("Users", UIImage.FromFile("User.png"), UIImage.FromFile("UserSelected.png"));

			TabBar.BarTintColor = SharedData.BackgroundColor;
			TabBar.TintColor = SharedData.TintColor;
		}
	}
}
