using System;
using System.Collections.Generic;
using System.Timers;
using Foundation;
using UIKit;
using CoreGraphics;

namespace LockIt
{
	public partial class SubPanelsTableViewController : UITableViewController
	{
		public CabinetData Cabinet;
		List<PanelData> PanelList;
		bool NoneFound;

		bool IsEditing;

		public SubPanelsTableViewController (IntPtr handle) : base (handle)
		{
		}

		public void Setup(CabinetData cabinet)
		{
			Cabinet = cabinet;
		}

		void SetIsEditing(bool isEditing)
		{
			if( NavigationItem != null )
			{
				NavigationItem.RightBarButtonItem = new UIBarButtonItem( (isEditing ? UIBarButtonSystemItem.Done : UIBarButtonSystemItem.Edit), (sender, e) =>
					{
						if( !IsEditing )
						{
							UIAlertController alert = UIAlertController.Create("Manually add or remove panels now...", "As you move them, they will be reflected below.", UIAlertControllerStyle.Alert);
							alert.AddAction( UIAlertAction.Create("OK", UIAlertActionStyle.Cancel, null ));
							PresentViewController(alert, true, null);
						}
							
						SetIsEditing(!IsEditing);
					});
			}
			IsEditing = isEditing;
		}

		#region View Controller Logic
		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			PanelList = new List<PanelData>();

			NavigationController.NavigationBar.BarTintColor = SharedData.BackgroundColor;

			SetIsEditing(false);

			RefreshControl = new UIRefreshControl();
			RefreshControl.ValueChanged += (sender, e) => 
			{
				if( RefreshControl.Refreshing )
				{
					RefreshControl.BeginRefreshing();

					NoneFound = false;
					TableView.ReloadData();

					Timer timer = new Timer(1500);
					timer.Elapsed += (s, ev) => 
					{
						InvokeOnMainThread( () =>
							{
								RefreshCallback();

								timer.Stop();
								timer = null;
							});
					};
					timer.Start();
				}
			};

			NoneFound = false;

			Timer firstTimer = new Timer(1500);
			firstTimer.Elapsed += (sender, e) => 
			{
				InvokeOnMainThread( () =>
					{
						RefreshCallback();

						firstTimer.Stop();
						firstTimer = null;
					});
			};
			firstTimer.Start();

			TableView.RegisterClassForCellReuse( typeof(PanelSummaryCell), new NSString("SummaryCell") );

			TableView.EstimatedRowHeight = 80;
			TableView.RowHeight = UITableView.AutomaticDimension;

			TableView.SeparatorStyle = UITableViewCellSeparatorStyle.None;
			TableView.SeparatorColor = UIColor.Clear;
		}
		private void RefreshCallback()
		{
			PanelList.Clear();

			NoneFound = true;

			foreach( var panel in Cabinet.PanelList )
			{
				if( panel.IsAllowed(SharedData.Instance.LoggedInUser) )
				{
					PanelList.Add(panel);
					NoneFound = false;
				}
			}

			if( RefreshControl != null && RefreshControl.Refreshing )
			{
				RefreshControl.EndRefreshing();
			}
			TableView.ReloadData();
		}

		bool first = true;

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);

			if( first )
			{
				TableView.ContentOffset = new CGPoint(0, TableView.ContentOffset.Y-RefreshControl.Frame.Height);
				RefreshControl.BeginRefreshing();
				first = false;
			}
		}

		public override void ViewDidDisappear(bool animated)
		{
			if( RefreshControl != null && RefreshControl.Refreshing )
			{
				RefreshControl.EndRefreshing();
			}

			base.ViewDidDisappear(animated);
		}
		#endregion View Controller Logic

		#region Table View Logic
		public override nint NumberOfSections(UITableView tableView)
		{
			return 1;
		}

		public override nint RowsInSection(UITableView tableview, nint section)
		{
			return PanelList.Count;
		}

		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
			PanelSummaryCell cell = (PanelSummaryCell)TableView.DequeueReusableCell(new NSString("SummaryCell"), indexPath);
			cell.Setup(PanelList[indexPath.Row]);
			return cell;
		}

		public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
		{
			KeysTableViewController toPush = (KeysTableViewController)Storyboard.InstantiateViewController("KeysTVC");
			toPush.Setup(PanelList[indexPath.Row]);
			NavigationController.PushViewController(toPush, true);
		}

		public override nfloat GetHeightForHeader(UITableView tableView, nint section)
		{
			return PanelList.Count == 0 ? 64 : 0;
		}

		public override UIView GetViewForHeader(UITableView tableView, nint section)
		{
			if( PanelList.Count == 0 )
			{
				UILabel headerView = new UILabel( new CGRect(0, 0, View.Bounds.Width, 64) );
				headerView.Font = UIFont.PreferredSubheadline;
				headerView.Text = NoneFound ? "No Panels Found" : "Getting list of Panels...";
				headerView.TextAlignment = UITextAlignment.Center;
				headerView.TextColor = SharedData.TintColor;
				return headerView;
			}
			return null;
		}
		#endregion Table View Logic
	}
}
