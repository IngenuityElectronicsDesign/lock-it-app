using System;
using System.Timers;
using Foundation;
using UIKit;

namespace LockIt
{
	public partial class LILoginViewController : UIViewController
	{
		public LILoginViewController(IntPtr handle) : base (handle)
		{
			
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			LogInButton.SetImage( UIImage.FromFile("LogIn.png").ImageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate), UIControlState.Normal );
			LogInButton.TintColor = SharedData.TintColor;

			UserNameTextField.ShouldReturn += (sender) =>
			{
				PasswordTextField.BecomeFirstResponder();
				return true;
			};

			PasswordTextField.ShouldReturn += (sender) =>
			{
				PasswordTextField.ResignFirstResponder();
				AttemptLogIn();
				return true;
			};

			// To speed up debugging
			UserNameTextField.Text = "admin";
			PasswordTextField.Text = "abcd1234";

			UserNameTextField.BecomeFirstResponder();
		}

		partial void InfoButtonPressed(UIKit.UIButton sender)
		{
			UIAlertView alert = new UIAlertView("", "Enter your username and password to access any of your authorised cabinets or panels in range.", null, "OK" );
			alert.Clicked += (s, e) => 
			{
				alert.DismissWithClickedButtonIndex(e.ButtonIndex, true);
			};
			alert.Show();
		}

		partial void LogInButtonPressed(UIKit.UIButton sender)
		{
			AttemptLogIn();
		}

		void AttemptLogIn()
		{
			ActivityIndicator.StartAnimating();
			LogInButton.Hidden = true;

			if( string.IsNullOrEmpty(UserNameTextField.Text) && string.IsNullOrEmpty(PasswordTextField.Text) )
			{
				LoginFailed();
				return;
			}

			bool success = false;

			foreach( var user in SharedData.Instance.UserList )
			{
				if( UserNameTextField.Text == user.Name && PasswordTextField.Text == user.Pass )
				{
					success = true;
					SharedData.Instance.LoggedInUser = user;
				}
			}

			Timer timer = new Timer(1500);
			timer.Elapsed += (sender, e) => 
			{
				InvokeOnMainThread( () =>
				{
					if( success )
					{
						LoginSuccess();
					}
					else
					{
						LoginFailed();	
					}

					timer.Stop();
					timer = null;
				});
			};
			timer.Start();
		}

		void LoginSuccess()
		{
			ActivityIndicator.StopAnimating();

			UIViewController toPresent = (UIViewController)Storyboard.InstantiateViewController("LITabBarC");
			PresentViewController(toPresent, true, () => { LogInButton.Hidden = false; } );
		}

		void LoginFailed()
		{
			ActivityIndicator.StopAnimating();
			LogInButton.Hidden = false;

			UIAlertView alert = new UIAlertView("Wrong username or password", "Please check your details, and contact your manager if you need assistance.", null, "OK" );
			alert.Clicked += (s, e) => 
			{
				alert.DismissWithClickedButtonIndex(e.ButtonIndex, true);
			};
			alert.Show();

			UserNameTextField.BecomeFirstResponder();
		}
	}
}
