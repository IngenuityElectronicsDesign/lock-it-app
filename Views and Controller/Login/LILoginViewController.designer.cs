// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace LockIt
{
	[Register ("LILoginViewController")]
	partial class LILoginViewController
	{
		[Outlet]
		UIKit.UIActivityIndicatorView ActivityIndicator { get; set; }

		[Outlet]
		UIKit.UIButton InfoButton { get; set; }

		[Outlet]
		UIKit.UIButton LogInButton { get; set; }

		[Outlet]
		UIKit.UITextField PasswordTextField { get; set; }

		[Outlet]
		UIKit.UITextField UserNameTextField { get; set; }

		[Action ("InfoButtonPressed:")]
		partial void InfoButtonPressed (UIKit.UIButton sender);

		[Action ("LogInButtonPressed:")]
		partial void LogInButtonPressed (UIKit.UIButton sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (ActivityIndicator != null) {
				ActivityIndicator.Dispose ();
				ActivityIndicator = null;
			}

			if (InfoButton != null) {
				InfoButton.Dispose ();
				InfoButton = null;
			}

			if (LogInButton != null) {
				LogInButton.Dispose ();
				LogInButton = null;
			}

			if (PasswordTextField != null) {
				PasswordTextField.Dispose ();
				PasswordTextField = null;
			}

			if (UserNameTextField != null) {
				UserNameTextField.Dispose ();
				UserNameTextField = null;
			}
		}
	}
}
