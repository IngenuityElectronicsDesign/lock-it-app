﻿using System;
using Foundation;
using UIKit;
using System.Collections.Generic;

namespace LockIt
{
	public class KeysResultsTableViewControllerController : UITableViewController
	{
		public List<KeyData> FilteredKeysList;

		public KeysResultsTableViewControllerController() : base(UITableViewStyle.Grouped)
		{
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			
			TableView.RegisterClassForCellReuse( typeof(KeySummaryCell), new NSString("SummaryCell") );

			TableView.EstimatedRowHeight = 80;
			TableView.RowHeight = UITableView.AutomaticDimension;

			TableView.SeparatorStyle = UITableViewCellSeparatorStyle.None;
			TableView.SeparatorColor = UIColor.Clear;
		}

		public override nint RowsInSection(UITableView tableview, nint section)
		{
			return FilteredKeysList.Count;
		}

		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
			KeySummaryCell cell = (KeySummaryCell)TableView.DequeueReusableCell(new NSString("SummaryCell"), indexPath);
			cell.Setup(FilteredKeysList[indexPath.Row]);
			return cell;
		}
	}
}

