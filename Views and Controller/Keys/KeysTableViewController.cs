using System;
using System.Linq;
using System.Collections.Generic;
using System.Timers;
using Foundation;
using UIKit;
using CoreGraphics;

namespace LockIt
{
	public partial class KeysTableViewController : UITableViewController
	{
		public enum SearchScopes
		{
			All,
			Name,
			ID,
			Fields,
			Field
		};

		public CabinetData Cabinet;
		public PanelData Panel;
		List<KeyData> KeyList;

		UISearchController SearchController;
		KeysResultsTableViewControllerController ResultsController;

		bool SearchControllerWasActive;
		bool SearchControllerSearchFieldWasFirstResponder;

		UIView SearchBarView;

		SearchScopes SearchScope;

		bool IsEditing;

		public KeysTableViewController(IntPtr handle) : base(handle)
		{
		}

		public void Setup(CabinetData cabinet)
		{
			Cabinet = cabinet;
		}

		public void Setup(PanelData panel)
		{
			Panel = panel;
		}

		void SetIsEditing(bool isEditing)
		{
			if( NavigationItem != null )
			{
				NavigationItem.RightBarButtonItem = new UIBarButtonItem( (isEditing ? UIBarButtonSystemItem.Done : UIBarButtonSystemItem.Edit), (sender, e) =>
					{
						if( !IsEditing )
						{
							UIAlertController alert = UIAlertController.Create("Manually add or remove keys now...", "As you move them, they will be reflected below.", UIAlertControllerStyle.Alert);
							alert.AddAction( UIAlertAction.Create("OK", UIAlertActionStyle.Cancel, null ));
							PresentViewController(alert, true, null);
						}

						SetIsEditing(!IsEditing);
					});
			}
			IsEditing = isEditing;
		}

		#region View Controller Logic
		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			KeyList = new List<KeyData>();

			SetIsEditing(false);

			RefreshControl = new UIRefreshControl();
			RefreshControl.ValueChanged += (sender, e) => 
			{
				if( RefreshControl.Refreshing )
				{
					RefreshControl.BeginRefreshing();

					Timer timer = new Timer(1500);
					timer.Elapsed += (s, ev) => 
					{
						InvokeOnMainThread( () =>
							{
								RefreshCallback();

								timer.Stop();
								timer = null;
							});
					};
					timer.Start();
				}
			};

			Timer firstTimer = new Timer(1500);
			firstTimer.Elapsed += (sender, e) => 
			{
				InvokeOnMainThread( () =>
					{
						RefreshCallback();

						firstTimer.Stop();
						firstTimer = null;
					});
			};
			firstTimer.Start();

			TableView.RegisterClassForCellReuse( typeof(KeySummaryCell), new NSString("SummaryCell") );

			TableView.EstimatedRowHeight = 80;
			TableView.RowHeight = UITableView.AutomaticDimension;

			TableView.SeparatorStyle = UITableViewCellSeparatorStyle.None;
			TableView.SeparatorColor = UIColor.Clear;

			ResultsController = new KeysResultsTableViewControllerController() { FilteredKeysList = new List<KeyData>() };
			ResultsController.View.BackgroundColor = View.BackgroundColor;

			SearchController = new UISearchController(ResultsController) { WeakDelegate = this, WeakSearchResultsUpdater = this, DimsBackgroundDuringPresentation = false };
			SearchController.SearchBar.SizeToFit();
			SearchController.SearchBar.ScopeButtonTitles = new string[] { "All", "Name", "ID", "Field" };
			SearchController.SearchBar.SelectedScopeButtonIndexChanged += (sender, e) => 
			{
				if( e.SelectedScope == 3 )
				{
					UIAlertController alert = UIAlertController.Create("Pick a Field to Search", "", UIAlertControllerStyle.ActionSheet);
					alert.AddAction(UIAlertAction.Create("Cancel", UIAlertActionStyle.Cancel, (alertAction) =>
						{
							SearchController.SearchBar.SelectedScopeButtonIndex = 0;
						}));

					foreach( var field in KeyList[0].Info.Keys.ToArray() )
					{
						alert.AddAction(UIAlertAction.Create(field, UIAlertActionStyle.Default, (alertAction) =>
							{
								SearchController.SearchBar.ScopeButtonTitles = new string[] { "All", "Name", "ID", "Field", field };
								SearchController.SearchBar.SelectedScopeButtonIndex = 4;
								SearchScope = (SearchScopes)(4);
							}));
					}

					PresentViewController(alert, true, null);
				}

				SearchScope = (SearchScopes)((int)e.SelectedScope);
			};
			SearchController.SearchBar.SearchButtonClicked += (sender, e) => 
			{
				((UISearchBar)sender).ResignFirstResponder();
			};

			SearchScope = SearchScopes.All;

			ResultsController.TableView.WeakDelegate = this;
			//SearchController.SearchBar.WeakDelegate = this;

			DefinesPresentationContext = true;

			if( SearchControllerWasActive )
			{
				SearchController.Active = SearchControllerWasActive;
				SearchControllerWasActive = false;

				if( SearchControllerSearchFieldWasFirstResponder )
				{
					SearchController.SearchBar.BecomeFirstResponder();
					SearchControllerSearchFieldWasFirstResponder = false;
				}
			}

			SearchBarView = new UIView(new CGRect(0, 0, SearchController.SearchBar.Bounds.Width, SearchController.SearchBar.Bounds.Height + 35));
			SearchBarView.AddSubview(SearchController.SearchBar);
			SearchController.SearchBar.TintColor = SharedData.TintColor;
			SearchController.SearchBar.BarTintColor = View.BackgroundColor;
		}

		private void RefreshCallback()
		{
			KeyList.Clear();

			List<KeyData> keyList = new List<KeyData>();

			if( Cabinet != null )
			{
				keyList = Cabinet.KeyList;
			}
			else if( Panel != null )
			{
				keyList = Panel.KeyList;
			}

			foreach( var key in keyList )
			{
				if( key.IsAllowed(SharedData.Instance.LoggedInUser) )
				{
					KeyList.Add(key);
				}
			}

			if( RefreshControl != null && RefreshControl.Refreshing )
			{
				RefreshControl.EndRefreshing();
			}
			TableView.ReloadData();
			TableView.TableHeaderView = SearchBarView;
		}

		bool first = true;

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);

			if( first )
			{
				TableView.ContentOffset = new CGPoint(0, TableView.ContentOffset.Y-RefreshControl.Frame.Height);
				RefreshControl.BeginRefreshing();
				first = false;
			}
		}

		public override void ViewDidDisappear(bool animated)
		{
			if( RefreshControl != null && RefreshControl.Refreshing )
			{
				RefreshControl.EndRefreshing();
			}

			base.ViewDidDisappear(animated);
		}
		#endregion View Controller Logic

		#region Table View Logic
		public override nint NumberOfSections(UITableView tableView)
		{
			return 1;
		}

		public override nint RowsInSection(UITableView tableview, nint section)
		{
			return KeyList.Count;
		}

		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
			KeySummaryCell cell = (KeySummaryCell)TableView.DequeueReusableCell(new NSString("SummaryCell"), indexPath);
			cell.Setup(KeyList[indexPath.Row]);
			return cell;
		}

		public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
		{
			KeysDetailTableViewController toPush = (KeysDetailTableViewController)Storyboard.InstantiateViewController("KeysDetailTVC");
			toPush.Setup(KeyList[indexPath.Row]);
			NavigationController.PushViewController(toPush, true);
		}

		public override nfloat GetHeightForHeader(UITableView tableView, nint section)
		{
			return (KeyList.Count == 0) ? 64 : 0;
		}

		public override UIView GetViewForHeader(UITableView tableView, nint section)
		{
			if( (KeyList.Count == 0) )
			{
				UILabel headerView = new UILabel( new CGRect(0, 0, View.Bounds.Width, 64) );
				headerView.Font = UIFont.PreferredSubheadline;
				headerView.Text = "Getting list of Keys...";
				headerView.TextAlignment = UITextAlignment.Center;
				headerView.TextColor = SharedData.TintColor;
				return headerView;
			}
			return null;
		}
		#endregion Table View Logic

		[Export ("updateSearchResultsForSearchController:")]
		public void UpdateSearchResultsForSearchController(UISearchController searchController)
		{
			var tableController = (KeysResultsTableViewControllerController)SearchController.SearchResultsController;
			tableController.FilteredKeysList = PerformSearch(searchController.SearchBar.Text);
			tableController.TableView.ReloadData();		
		}

		List<KeyData> PerformSearch(string searchString)
		{
			searchString = searchString.Trim();

			string[] searchItems = string.IsNullOrEmpty(searchString) ? new string[0] : searchString.Split( new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

			var filteredKeys = new List<KeyData>();

			foreach( var key in KeyList )
			{
				foreach( var item in searchItems )
				{
					if( SearchScope == SearchScopes.All )
					{
						if( key.GetSearchString().ToLower().Contains( item.ToLower() ) )
						{
							filteredKeys.Add(key);
							break;
						}
					}
					else if( SearchScope == SearchScopes.Name )
					{
						if( key.Name.ToLower().Contains( item.ToLower() ) )
						{
							filteredKeys.Add(key);
							break;
						}
					}
					else if( SearchScope == SearchScopes.ID )
					{
						if( key.UUID.ToLower().Contains( item.ToLower() ) )
						{
							filteredKeys.Add(key);
							break;
						}
					}
					else if( SearchScope == SearchScopes.Field )
					{
						if( key.Name.ToLower().Contains( item.ToLower() ) )
						{
							filteredKeys.Add(key);
							break;
						}
					}
				}
			}

			return filteredKeys.Distinct().ToList();
		}
	}
}
