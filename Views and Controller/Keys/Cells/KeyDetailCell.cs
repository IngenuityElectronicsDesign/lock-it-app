﻿using System;
using Foundation;
using UIKit;
using CoreGraphics;

namespace LockIt
{
	public class KeyDetailCell : UITableViewCell
	{
		//public KeyData Data { get; private set; }

		UIView ContainerView;

		UITextField KeyTextField;
		UITextField ValueTextField;

		UIImageView DisclosureImage;

		bool CanEditKey;

		public KeyDetailCell(IntPtr handle) : base(handle)
		{
			SelectionStyle = UITableViewCellSelectionStyle.None;
			BackgroundColor = UIColor.Clear;

			ContainerView = new UIView(CGRect.Empty);
			ContainerView.BackgroundColor = UIColor.White;
			ContainerView.Layer.CornerRadius = 5;
			ContainerView.Layer.MasksToBounds = true;
			AddSubview(ContainerView);

			KeyTextField = new UITextField(CGRect.Empty);
			KeyTextField.Font = UIFont.PreferredHeadline;
			KeyTextField.Text = "Key";
			KeyTextField.TextColor = SharedData.TintColor;
			KeyTextField.BorderStyle = UITextBorderStyle.None;
			ContainerView.AddSubview(KeyTextField);

			ValueTextField = new UITextField(CGRect.Empty);
			ValueTextField.Font = UIFont.PreferredSubheadline;
			ValueTextField.Text = "Value";
			ValueTextField.TextAlignment = UITextAlignment.Right;
			ValueTextField.BorderStyle = UITextBorderStyle.None;
			ContainerView.AddSubview(ValueTextField);

			DisclosureImage = new UIImageView(new CGRect(Bounds.Width-12, 12, 20, 20));
			DisclosureImage.Image = UIImage.FromFile("Disclosure.png").ImageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate);
			DisclosureImage.TintColor = SharedData.TintColor;
			ContainerView.AddSubview(DisclosureImage);
		}

		public void Setup(string key, string value, bool canEditKey, bool showDisclosure) //KeyData data)
		{
			//Data = data;

			KeyTextField.Text = key;
			ValueTextField.Text = value;

			CanEditKey = canEditKey;

			KeyTextField.Enabled = CanEditKey;
			ValueTextField.Enabled = !showDisclosure;
			DisclosureImage.Hidden = !showDisclosure;
		}

		public override CGSize SizeThatFits(CGSize size)
		{
			size.Height = 50;

			Position(size);

			return size;
		}

		public void Position(CGSize size)
		{
			ContainerView.Frame = new CGRect(10, 0, size.Width-20, size.Height-10);

			KeyTextField.Frame = new CGRect(10f, 10f, ContainerView.Bounds.Width/2, 20f);
			ValueTextField.Frame = new CGRect(ContainerView.Bounds.Width/2, 10f, ContainerView.Bounds.Width/2-30f, 20f);

			DisclosureImage.Frame = new CGRect(ContainerView.Bounds.Width-25, 10, 20, 20);
		}

		public override void SetSelected(bool selected, bool animated)
		{
			base.SetSelected(false, false);
		}
	}
}
