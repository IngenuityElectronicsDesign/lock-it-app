using System;
using System.Collections.Generic;
using System.Linq;
using System.Timers;
using Foundation;
using UIKit;
using CoreGraphics;

namespace LockIt
{
	public partial class KeysDetailTableViewController : UITableViewController
	{
		public KeyData Data;

		public KeysDetailTableViewController(IntPtr handle) : base(handle)
		{
		}

		public void Setup(KeyData data)
		{
			Data = data;
		}

		#region View Controller Logic
		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			Title = Data.Name;

			TableView.RegisterClassForCellReuse( typeof(KeyDetailCell), new NSString("DetailCell") );

			TableView.EstimatedRowHeight = 50;
			TableView.RowHeight = UITableView.AutomaticDimension;

			TableView.SeparatorStyle = UITableViewCellSeparatorStyle.None;
			TableView.SeparatorColor = UIColor.Clear;
		}

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);
		}

		public override void ViewDidDisappear(bool animated)
		{
			base.ViewDidDisappear(animated);
		}
		#endregion View Controller Logic

		#region Table View Logic
		public override nint NumberOfSections(UITableView tableView)
		{
			return 3;
		}

		public override nint RowsInSection(UITableView tableview, nint section)
		{
			if( section == 0 )
			{
				return 2;
			}
			else if( section == 1 )
			{
				return Data.Info.Count + 1;	
			}
			return 1;
		}

		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
			KeyDetailCell cell = (KeyDetailCell)TableView.DequeueReusableCell(new NSString("DetailCell"), indexPath);

			if( indexPath.Section == 0 )
			{
				if( indexPath.Row == 0 )
				{
					cell.Setup("Name", Data.Name, false, false);
				}
				else
				{
					cell.Setup("UUID", Data.UUID, false, false);
				}
			}
			else if( indexPath.Section == 1 )
			{
				if( indexPath.Row == Data.Info.Count )
				{
					cell.Setup("", "", true, false);
				}
				else
				{
					var key = Data.Info.Keys.ToArray()[indexPath.Row];
					var value = Data.Info[key];
					cell.Setup(key, value, true, false);	
				}
			}
			else
			{
				cell.Setup("View Access History", "", false, true);
			}

			return cell;
		}

		public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
		{
			if( indexPath.Section == 2 )
			{
				AuditTrailTableViewController toPush = (AuditTrailTableViewController)Storyboard.InstantiateViewController("AuditTrailTVC");
				toPush.Setup(Data.Name, Data.AuditTrail);
				NavigationController.PushViewController(toPush, true);
			}
		}
		#endregion Table View Logic
	}
}
