using System;
using CoreGraphics;
using System.Collections.Generic;
using Foundation;
using UIKit;

namespace LockIt
{
	public class PatternUnlockSliderView : UIControl, IPatternUnlockNubViewDelegate
	{
		bool Unlocked = false;

		UIImage BackgroundUnlockedImage;

		UIImageView BackgroundView;
		PatternUnlockNubView NubView;

		Action Completion;
		Action Timeout;

		List<CGPoint> Points;

		public PatternUnlockSliderView(CGRect frame, PatternUnlockPattern container, Action completion, Action timeout) : base(frame)
		{
			Points = container.Points;

			BackgroundUnlockedImage = container.BlueImage;
			Completion = completion;
			Timeout = timeout;

			BackgroundView = new UIImageView(new CGRect(new CGPoint(), Frame.Size));
			BackgroundView.Image = container.RedImage;
			AddSubview(BackgroundView);

			NubView = new PatternUnlockNubView(Points[0]);
			NubView.Delegate = this;
			NubView.SetPathBounds(Points[0], Points[1], 1);
			AddSubview(NubView);
		}

		#region ISPPatternUnlockNubViewDelegate implementation
		public void ReachedEndPoint(int pointIndex, Action finalCompletion)
		{
			if( pointIndex == Points.Count-1 )
			{
				NubView.SetPathBounds(Points[pointIndex], Points[pointIndex], pointIndex);
				BackgroundView.Image = BackgroundUnlockedImage;

				if( Completion != null && !Unlocked )
				{
					Unlocked = true;
					Completion();

					if( finalCompletion != null )
					{
						finalCompletion();
					}
				}
			}
			else
			{
				NubView.SetPathBounds(Points[pointIndex], Points[pointIndex+1], pointIndex+1);
			}
		}

		public void FingerLiftedTimeout()
		{
			if( Timeout != null )
			{
				Timeout();
			}
		}
		#endregion ISPPatternUnlockNubViewDelegate implementation
	}

	public class PatternUnlockPattern
	{
		public UIImage RedImage { get; private set; }
		public UIImage BlueImage { get; private set; }
		public List<CGPoint> Points { get; private set; }

		public PatternUnlockPattern(UIImage redImage, UIImage blueImage, List<CGPoint> points)
		{
			RedImage = redImage;
			BlueImage = blueImage;
			Points = points;
		}
	}
}
