using System;
using CoreGraphics;
using System.Collections.Generic;
using System.Timers;
using Foundation;
using CoreFoundation;
using UIKit;

namespace LockIt
{
	public interface IPatternUnlockNubViewDelegate
	{
		void ReachedEndPoint(int pointIndex, Action finalCompletion);
		void FingerLiftedTimeout();
	} 

	public class PatternUnlockNubView : UIControl
	{
		public IPatternUnlockNubViewDelegate Delegate { get; set; }
		int PointIndex;
		bool DirectionIsDown;
		bool IsDiagonal;

		UIImageView BackgroundView;
		UIPanGestureRecognizer PanGestureRecogniser;

		public CGPoint MinBounds { get; private set; }
		public CGPoint MaxBounds { get; private set; }

		static float Radius = 20f;
		static CGSize Size = new CGSize(40f, 40f);

		Timer FingerLiftedTimer;
		bool TimeoutAllowed;

		public PatternUnlockNubView(CGPoint location) : base(new CGRect(location, Size))
		{
			TimeoutAllowed = true;

			BackgroundView = new UIImageView(new CGRect(3f, 3f, 34f, 34f));
			BackgroundView.Image = UIImage.FromFile("AirUnlock/slider.png");
			AddSubview(BackgroundView);

			PanGestureRecogniser = new UIPanGestureRecognizer( () =>
				{
					CGPoint translation = PanGestureRecogniser.TranslationInView(Superview);

					double X = Center.X + translation.X;
					double Y = Center.Y + translation.Y;

					X = Math.Min(Math.Max(X, MinBounds.X+Radius), MaxBounds.X-Radius);
					Y = Math.Min(Math.Max(Y, MinBounds.Y+Radius), MaxBounds.Y-Radius);

					if( IsDiagonal )
					{
						double ratio = ( ((MaxBounds.Y-Radius*2)-MinBounds.Y) / ((MaxBounds.X-Radius*2)-MinBounds.X) );
						Y = (DirectionIsDown ? 1 : -1) * ( ratio*(X-(MinBounds.X+Radius))) + (DirectionIsDown ? Radius : (MaxBounds.Y-Radius));
					}

					Center = new CGPoint((float)Math.Round(X), (float)Math.Round(Y));

					if( Center.X >= MaxBounds.X-(Radius+3f) && (DirectionIsDown ? Center.Y >= MaxBounds.Y-(Radius+3f) : Center.Y <= MinBounds.Y+(Radius+3f)) )
					{
						Delegate.ReachedEndPoint(PointIndex, () =>
							{
								SetFingerLiftedTimer(false);
								TimeoutAllowed = false;
							});
					}
//					else if( Center == new PointF(MinBounds.X+Radius, (DirectionIsDown ? MinBounds.Y+Radius : MaxBounds.Y-Radius)) )
//					{
//						Console.WriteLine("Start");
//					}

					PanGestureRecogniser.SetTranslation(new CGPoint(), Superview);

					if( PanGestureRecogniser.NumberOfTouches > 0 )
					{
						SetFingerLiftedTimer(false);
						CGPoint locationInNib = PanGestureRecogniser.LocationOfTouch(0, this);

						if( locationInNib.X < -Radius*0.5f || locationInNib.X > Frame.Width+Radius*0.5f || locationInNib.Y < -Radius*0.5f || locationInNib.Y > Frame.Height+Radius*0.5f )
						{
							PanGestureRecogniser.Enabled = false;
							PanGestureRecogniser.Enabled = true;
						}
					}
					else
					{
						SetFingerLiftedTimer(true);
					}
				});

			PanGestureRecogniser.MaximumNumberOfTouches = 1;
			PanGestureRecogniser.MinimumNumberOfTouches = 1;
			PanGestureRecogniser.CancelsTouchesInView = false;

			AddGestureRecognizer(PanGestureRecogniser);
		}

		public void SetPathBounds(CGPoint startPoint, CGPoint endPoint, int pointIndex)
		{
			PointIndex = pointIndex;
			DirectionIsDown = (startPoint.Y <= endPoint.Y);
			IsDiagonal = (startPoint.X != endPoint.X && startPoint.Y != endPoint.Y);

			nfloat X = (nfloat)Math.Min(startPoint.X, endPoint.X);
			nfloat Y = (nfloat)Math.Min(startPoint.Y, endPoint.Y);

			MinBounds = new CGPoint(X, Y);

			X = (nfloat)Math.Max(startPoint.X+2*Radius, endPoint.X+2*Radius);
			Y = (nfloat)Math.Max(startPoint.Y+2*Radius, endPoint.Y+2*Radius);

			MaxBounds = new CGPoint(X, Y);
		}

		private void SetFingerLiftedTimer(bool start)
		{
			if( start && FingerLiftedTimer == null && TimeoutAllowed )
			{
				FingerLiftedTimer = new Timer(2000); // 2 Second Timeout
				FingerLiftedTimer.Elapsed += (sender, e) => 
				{
					DispatchQueue.MainQueue.DispatchAsync( () =>
						{
							Delegate.FingerLiftedTimeout();
						});
					FingerLiftedTimer.Stop();
					FingerLiftedTimer = null;
				};
				FingerLiftedTimer.Start();
			}
			else if( !start && FingerLiftedTimer != null )
			{
				FingerLiftedTimer.Stop();
				FingerLiftedTimer = null;
			}
		}
	}
}
